/*
 * generated by Xtext
 */
package org.xtext.example.mydsl.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the IDE.
 */
public class DaehUiModule extends org.xtext.example.mydsl.ui.AbstractDaehUiModule {
	public DaehUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
