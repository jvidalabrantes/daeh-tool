package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.DaehGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDaehParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'full'", "'partial'", "'*'", "'ehrule'", "'{'", "'type'", "'='", "','", "'signaler'", "'exceptionHandler'", "'}'", "';'", "'['", "'exception'", "'handler = '", "']'", "'.'", "'.*'", "'(..)'", "'+'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__30=30;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalDaehParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDaehParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDaehParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g"; }


     
     	private DaehGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(DaehGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleModel"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:60:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:61:1: ( ruleModel EOF )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:62:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel61);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:69:1: ruleModel : ( ( rule__Model__GreetingsAssignment )* ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:73:2: ( ( ( rule__Model__GreetingsAssignment )* ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:74:1: ( ( rule__Model__GreetingsAssignment )* )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:74:1: ( ( rule__Model__GreetingsAssignment )* )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:75:1: ( rule__Model__GreetingsAssignment )*
            {
             before(grammarAccess.getModelAccess().getGreetingsAssignment()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:76:1: ( rule__Model__GreetingsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:76:2: rule__Model__GreetingsAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Model__GreetingsAssignment_in_ruleModel94);
            	    rule__Model__GreetingsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getGreetingsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleGreeting"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:88:1: entryRuleGreeting : ruleGreeting EOF ;
    public final void entryRuleGreeting() throws RecognitionException {
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:89:1: ( ruleGreeting EOF )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:90:1: ruleGreeting EOF
            {
             before(grammarAccess.getGreetingRule()); 
            pushFollow(FOLLOW_ruleGreeting_in_entryRuleGreeting122);
            ruleGreeting();

            state._fsp--;

             after(grammarAccess.getGreetingRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGreeting129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGreeting"


    // $ANTLR start "ruleGreeting"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:97:1: ruleGreeting : ( ( rule__Greeting__Group__0 ) ) ;
    public final void ruleGreeting() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:101:2: ( ( ( rule__Greeting__Group__0 ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:102:1: ( ( rule__Greeting__Group__0 ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:102:1: ( ( rule__Greeting__Group__0 ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:103:1: ( rule__Greeting__Group__0 )
            {
             before(grammarAccess.getGreetingAccess().getGroup()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:104:1: ( rule__Greeting__Group__0 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:104:2: rule__Greeting__Group__0
            {
            pushFollow(FOLLOW_rule__Greeting__Group__0_in_ruleGreeting155);
            rule__Greeting__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGreetingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGreeting"


    // $ANTLR start "entryRuleExceptionHandler"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:116:1: entryRuleExceptionHandler : ruleExceptionHandler EOF ;
    public final void entryRuleExceptionHandler() throws RecognitionException {
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:117:1: ( ruleExceptionHandler EOF )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:118:1: ruleExceptionHandler EOF
            {
             before(grammarAccess.getExceptionHandlerRule()); 
            pushFollow(FOLLOW_ruleExceptionHandler_in_entryRuleExceptionHandler182);
            ruleExceptionHandler();

            state._fsp--;

             after(grammarAccess.getExceptionHandlerRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExceptionHandler189); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExceptionHandler"


    // $ANTLR start "ruleExceptionHandler"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:125:1: ruleExceptionHandler : ( ( rule__ExceptionHandler__Group__0 ) ) ;
    public final void ruleExceptionHandler() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:129:2: ( ( ( rule__ExceptionHandler__Group__0 ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:130:1: ( ( rule__ExceptionHandler__Group__0 ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:130:1: ( ( rule__ExceptionHandler__Group__0 ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:131:1: ( rule__ExceptionHandler__Group__0 )
            {
             before(grammarAccess.getExceptionHandlerAccess().getGroup()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:132:1: ( rule__ExceptionHandler__Group__0 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:132:2: rule__ExceptionHandler__Group__0
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__0_in_ruleExceptionHandler215);
            rule__ExceptionHandler__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExceptionHandlerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExceptionHandler"


    // $ANTLR start "entryRuleTypeRule"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:144:1: entryRuleTypeRule : ruleTypeRule EOF ;
    public final void entryRuleTypeRule() throws RecognitionException {
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:145:1: ( ruleTypeRule EOF )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:146:1: ruleTypeRule EOF
            {
             before(grammarAccess.getTypeRuleRule()); 
            pushFollow(FOLLOW_ruleTypeRule_in_entryRuleTypeRule242);
            ruleTypeRule();

            state._fsp--;

             after(grammarAccess.getTypeRuleRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeRule249); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeRule"


    // $ANTLR start "ruleTypeRule"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:153:1: ruleTypeRule : ( ( rule__TypeRule__Alternatives ) ) ;
    public final void ruleTypeRule() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:157:2: ( ( ( rule__TypeRule__Alternatives ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:158:1: ( ( rule__TypeRule__Alternatives ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:158:1: ( ( rule__TypeRule__Alternatives ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:159:1: ( rule__TypeRule__Alternatives )
            {
             before(grammarAccess.getTypeRuleAccess().getAlternatives()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:160:1: ( rule__TypeRule__Alternatives )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:160:2: rule__TypeRule__Alternatives
            {
            pushFollow(FOLLOW_rule__TypeRule__Alternatives_in_ruleTypeRule275);
            rule__TypeRule__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeRuleAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeRule"


    // $ANTLR start "entryRuleQualifiedName"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:172:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:173:1: ( ruleQualifiedName EOF )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:174:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName302);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName309); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:181:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:185:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:186:1: ( ( rule__QualifiedName__Group__0 ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:186:1: ( ( rule__QualifiedName__Group__0 ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:187:1: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:188:1: ( rule__QualifiedName__Group__0 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:188:2: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName335);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:200:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:201:1: ( ruleQualifiedNameWithWildcard EOF )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:202:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard362);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard369); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:209:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Alternatives ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:213:2: ( ( ( rule__QualifiedNameWithWildcard__Alternatives ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:214:1: ( ( rule__QualifiedNameWithWildcard__Alternatives ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:214:1: ( ( rule__QualifiedNameWithWildcard__Alternatives ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:215:1: ( rule__QualifiedNameWithWildcard__Alternatives )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getAlternatives()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:216:1: ( rule__QualifiedNameWithWildcard__Alternatives )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:216:2: rule__QualifiedNameWithWildcard__Alternatives
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Alternatives_in_ruleQualifiedNameWithWildcard395);
            rule__QualifiedNameWithWildcard__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedNameWithWildcardSignaler"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:228:1: entryRuleQualifiedNameWithWildcardSignaler : ruleQualifiedNameWithWildcardSignaler EOF ;
    public final void entryRuleQualifiedNameWithWildcardSignaler() throws RecognitionException {
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:229:1: ( ruleQualifiedNameWithWildcardSignaler EOF )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:230:1: ruleQualifiedNameWithWildcardSignaler EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardSignalerRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_entryRuleQualifiedNameWithWildcardSignaler422);
            ruleQualifiedNameWithWildcardSignaler();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardSignalerRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcardSignaler429); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcardSignaler"


    // $ANTLR start "ruleQualifiedNameWithWildcardSignaler"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:237:1: ruleQualifiedNameWithWildcardSignaler : ( ( rule__QualifiedNameWithWildcardSignaler__Alternatives ) ) ;
    public final void ruleQualifiedNameWithWildcardSignaler() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:241:2: ( ( ( rule__QualifiedNameWithWildcardSignaler__Alternatives ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:242:1: ( ( rule__QualifiedNameWithWildcardSignaler__Alternatives ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:242:1: ( ( rule__QualifiedNameWithWildcardSignaler__Alternatives ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:243:1: ( rule__QualifiedNameWithWildcardSignaler__Alternatives )
            {
             before(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAlternatives()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:244:1: ( rule__QualifiedNameWithWildcardSignaler__Alternatives )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:244:2: rule__QualifiedNameWithWildcardSignaler__Alternatives
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcardSignaler__Alternatives_in_ruleQualifiedNameWithWildcardSignaler455);
            rule__QualifiedNameWithWildcardSignaler__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcardSignaler"


    // $ANTLR start "rule__TypeRule__Alternatives"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:256:1: rule__TypeRule__Alternatives : ( ( 'full' ) | ( 'partial' ) );
    public final void rule__TypeRule__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:260:1: ( ( 'full' ) | ( 'partial' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:261:1: ( 'full' )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:261:1: ( 'full' )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:262:1: 'full'
                    {
                     before(grammarAccess.getTypeRuleAccess().getFullKeyword_0()); 
                    match(input,11,FOLLOW_11_in_rule__TypeRule__Alternatives492); 
                     after(grammarAccess.getTypeRuleAccess().getFullKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:269:6: ( 'partial' )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:269:6: ( 'partial' )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:270:1: 'partial'
                    {
                     before(grammarAccess.getTypeRuleAccess().getPartialKeyword_1()); 
                    match(input,12,FOLLOW_12_in_rule__TypeRule__Alternatives512); 
                     after(grammarAccess.getTypeRuleAccess().getPartialKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeRule__Alternatives"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Alternatives"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:282:1: rule__QualifiedNameWithWildcard__Alternatives : ( ( ruleQualifiedName ) | ( ( rule__QualifiedNameWithWildcard__Group_1__0 ) ) | ( ( rule__QualifiedNameWithWildcard__Group_2__0 ) ) | ( ( rule__QualifiedNameWithWildcard__Group_3__0 ) ) );
    public final void rule__QualifiedNameWithWildcard__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:286:1: ( ( ruleQualifiedName ) | ( ( rule__QualifiedNameWithWildcard__Group_1__0 ) ) | ( ( rule__QualifiedNameWithWildcard__Group_2__0 ) ) | ( ( rule__QualifiedNameWithWildcard__Group_3__0 ) ) )
            int alt3=4;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:287:1: ( ruleQualifiedName )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:287:1: ( ruleQualifiedName )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:288:1: ruleQualifiedName
                    {
                     before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Alternatives546);
                    ruleQualifiedName();

                    state._fsp--;

                     after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:293:6: ( ( rule__QualifiedNameWithWildcard__Group_1__0 ) )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:293:6: ( ( rule__QualifiedNameWithWildcard__Group_1__0 ) )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:294:1: ( rule__QualifiedNameWithWildcard__Group_1__0 )
                    {
                     before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_1()); 
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:295:1: ( rule__QualifiedNameWithWildcard__Group_1__0 )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:295:2: rule__QualifiedNameWithWildcard__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_1__0_in_rule__QualifiedNameWithWildcard__Alternatives563);
                    rule__QualifiedNameWithWildcard__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:299:6: ( ( rule__QualifiedNameWithWildcard__Group_2__0 ) )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:299:6: ( ( rule__QualifiedNameWithWildcard__Group_2__0 ) )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:300:1: ( rule__QualifiedNameWithWildcard__Group_2__0 )
                    {
                     before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_2()); 
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:301:1: ( rule__QualifiedNameWithWildcard__Group_2__0 )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:301:2: rule__QualifiedNameWithWildcard__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_2__0_in_rule__QualifiedNameWithWildcard__Alternatives581);
                    rule__QualifiedNameWithWildcard__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:305:6: ( ( rule__QualifiedNameWithWildcard__Group_3__0 ) )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:305:6: ( ( rule__QualifiedNameWithWildcard__Group_3__0 ) )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:306:1: ( rule__QualifiedNameWithWildcard__Group_3__0 )
                    {
                     before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_3()); 
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:307:1: ( rule__QualifiedNameWithWildcard__Group_3__0 )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:307:2: rule__QualifiedNameWithWildcard__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_3__0_in_rule__QualifiedNameWithWildcard__Alternatives599);
                    rule__QualifiedNameWithWildcard__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Alternatives"


    // $ANTLR start "rule__QualifiedNameWithWildcardSignaler__Alternatives"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:316:1: rule__QualifiedNameWithWildcardSignaler__Alternatives : ( ( ruleQualifiedNameWithWildcard ) | ( '*' ) );
    public final void rule__QualifiedNameWithWildcardSignaler__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:320:1: ( ( ruleQualifiedNameWithWildcard ) | ( '*' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==13) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:321:1: ( ruleQualifiedNameWithWildcard )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:321:1: ( ruleQualifiedNameWithWildcard )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:322:1: ruleQualifiedNameWithWildcard
                    {
                     before(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getQualifiedNameWithWildcardParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_rule__QualifiedNameWithWildcardSignaler__Alternatives632);
                    ruleQualifiedNameWithWildcard();

                    state._fsp--;

                     after(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getQualifiedNameWithWildcardParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:327:6: ( '*' )
                    {
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:327:6: ( '*' )
                    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:328:1: '*'
                    {
                     before(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAsteriskKeyword_1()); 
                    match(input,13,FOLLOW_13_in_rule__QualifiedNameWithWildcardSignaler__Alternatives650); 
                     after(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAsteriskKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcardSignaler__Alternatives"


    // $ANTLR start "rule__Greeting__Group__0"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:342:1: rule__Greeting__Group__0 : rule__Greeting__Group__0__Impl rule__Greeting__Group__1 ;
    public final void rule__Greeting__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:346:1: ( rule__Greeting__Group__0__Impl rule__Greeting__Group__1 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:347:2: rule__Greeting__Group__0__Impl rule__Greeting__Group__1
            {
            pushFollow(FOLLOW_rule__Greeting__Group__0__Impl_in_rule__Greeting__Group__0682);
            rule__Greeting__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__1_in_rule__Greeting__Group__0685);
            rule__Greeting__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__0"


    // $ANTLR start "rule__Greeting__Group__0__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:354:1: rule__Greeting__Group__0__Impl : ( 'ehrule' ) ;
    public final void rule__Greeting__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:358:1: ( ( 'ehrule' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:359:1: ( 'ehrule' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:359:1: ( 'ehrule' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:360:1: 'ehrule'
            {
             before(grammarAccess.getGreetingAccess().getEhruleKeyword_0()); 
            match(input,14,FOLLOW_14_in_rule__Greeting__Group__0__Impl713); 
             after(grammarAccess.getGreetingAccess().getEhruleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__0__Impl"


    // $ANTLR start "rule__Greeting__Group__1"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:373:1: rule__Greeting__Group__1 : rule__Greeting__Group__1__Impl rule__Greeting__Group__2 ;
    public final void rule__Greeting__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:377:1: ( rule__Greeting__Group__1__Impl rule__Greeting__Group__2 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:378:2: rule__Greeting__Group__1__Impl rule__Greeting__Group__2
            {
            pushFollow(FOLLOW_rule__Greeting__Group__1__Impl_in_rule__Greeting__Group__1744);
            rule__Greeting__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__2_in_rule__Greeting__Group__1747);
            rule__Greeting__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__1"


    // $ANTLR start "rule__Greeting__Group__1__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:385:1: rule__Greeting__Group__1__Impl : ( '{' ) ;
    public final void rule__Greeting__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:389:1: ( ( '{' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:390:1: ( '{' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:390:1: ( '{' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:391:1: '{'
            {
             before(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,15,FOLLOW_15_in_rule__Greeting__Group__1__Impl775); 
             after(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__1__Impl"


    // $ANTLR start "rule__Greeting__Group__2"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:404:1: rule__Greeting__Group__2 : rule__Greeting__Group__2__Impl rule__Greeting__Group__3 ;
    public final void rule__Greeting__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:408:1: ( rule__Greeting__Group__2__Impl rule__Greeting__Group__3 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:409:2: rule__Greeting__Group__2__Impl rule__Greeting__Group__3
            {
            pushFollow(FOLLOW_rule__Greeting__Group__2__Impl_in_rule__Greeting__Group__2806);
            rule__Greeting__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__3_in_rule__Greeting__Group__2809);
            rule__Greeting__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__2"


    // $ANTLR start "rule__Greeting__Group__2__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:416:1: rule__Greeting__Group__2__Impl : ( 'type' ) ;
    public final void rule__Greeting__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:420:1: ( ( 'type' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:421:1: ( 'type' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:421:1: ( 'type' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:422:1: 'type'
            {
             before(grammarAccess.getGreetingAccess().getTypeKeyword_2()); 
            match(input,16,FOLLOW_16_in_rule__Greeting__Group__2__Impl837); 
             after(grammarAccess.getGreetingAccess().getTypeKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__2__Impl"


    // $ANTLR start "rule__Greeting__Group__3"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:435:1: rule__Greeting__Group__3 : rule__Greeting__Group__3__Impl rule__Greeting__Group__4 ;
    public final void rule__Greeting__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:439:1: ( rule__Greeting__Group__3__Impl rule__Greeting__Group__4 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:440:2: rule__Greeting__Group__3__Impl rule__Greeting__Group__4
            {
            pushFollow(FOLLOW_rule__Greeting__Group__3__Impl_in_rule__Greeting__Group__3868);
            rule__Greeting__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__4_in_rule__Greeting__Group__3871);
            rule__Greeting__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__3"


    // $ANTLR start "rule__Greeting__Group__3__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:447:1: rule__Greeting__Group__3__Impl : ( '=' ) ;
    public final void rule__Greeting__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:451:1: ( ( '=' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:452:1: ( '=' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:452:1: ( '=' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:453:1: '='
            {
             before(grammarAccess.getGreetingAccess().getEqualsSignKeyword_3()); 
            match(input,17,FOLLOW_17_in_rule__Greeting__Group__3__Impl899); 
             after(grammarAccess.getGreetingAccess().getEqualsSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__3__Impl"


    // $ANTLR start "rule__Greeting__Group__4"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:466:1: rule__Greeting__Group__4 : rule__Greeting__Group__4__Impl rule__Greeting__Group__5 ;
    public final void rule__Greeting__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:470:1: ( rule__Greeting__Group__4__Impl rule__Greeting__Group__5 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:471:2: rule__Greeting__Group__4__Impl rule__Greeting__Group__5
            {
            pushFollow(FOLLOW_rule__Greeting__Group__4__Impl_in_rule__Greeting__Group__4930);
            rule__Greeting__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__5_in_rule__Greeting__Group__4933);
            rule__Greeting__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__4"


    // $ANTLR start "rule__Greeting__Group__4__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:478:1: rule__Greeting__Group__4__Impl : ( ( rule__Greeting__TypeContractAssignment_4 ) ) ;
    public final void rule__Greeting__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:482:1: ( ( ( rule__Greeting__TypeContractAssignment_4 ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:483:1: ( ( rule__Greeting__TypeContractAssignment_4 ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:483:1: ( ( rule__Greeting__TypeContractAssignment_4 ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:484:1: ( rule__Greeting__TypeContractAssignment_4 )
            {
             before(grammarAccess.getGreetingAccess().getTypeContractAssignment_4()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:485:1: ( rule__Greeting__TypeContractAssignment_4 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:485:2: rule__Greeting__TypeContractAssignment_4
            {
            pushFollow(FOLLOW_rule__Greeting__TypeContractAssignment_4_in_rule__Greeting__Group__4__Impl960);
            rule__Greeting__TypeContractAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getGreetingAccess().getTypeContractAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__4__Impl"


    // $ANTLR start "rule__Greeting__Group__5"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:495:1: rule__Greeting__Group__5 : rule__Greeting__Group__5__Impl rule__Greeting__Group__6 ;
    public final void rule__Greeting__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:499:1: ( rule__Greeting__Group__5__Impl rule__Greeting__Group__6 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:500:2: rule__Greeting__Group__5__Impl rule__Greeting__Group__6
            {
            pushFollow(FOLLOW_rule__Greeting__Group__5__Impl_in_rule__Greeting__Group__5990);
            rule__Greeting__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__6_in_rule__Greeting__Group__5993);
            rule__Greeting__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__5"


    // $ANTLR start "rule__Greeting__Group__5__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:507:1: rule__Greeting__Group__5__Impl : ( ',' ) ;
    public final void rule__Greeting__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:511:1: ( ( ',' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:512:1: ( ',' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:512:1: ( ',' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:513:1: ','
            {
             before(grammarAccess.getGreetingAccess().getCommaKeyword_5()); 
            match(input,18,FOLLOW_18_in_rule__Greeting__Group__5__Impl1021); 
             after(grammarAccess.getGreetingAccess().getCommaKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__5__Impl"


    // $ANTLR start "rule__Greeting__Group__6"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:526:1: rule__Greeting__Group__6 : rule__Greeting__Group__6__Impl rule__Greeting__Group__7 ;
    public final void rule__Greeting__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:530:1: ( rule__Greeting__Group__6__Impl rule__Greeting__Group__7 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:531:2: rule__Greeting__Group__6__Impl rule__Greeting__Group__7
            {
            pushFollow(FOLLOW_rule__Greeting__Group__6__Impl_in_rule__Greeting__Group__61052);
            rule__Greeting__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__7_in_rule__Greeting__Group__61055);
            rule__Greeting__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__6"


    // $ANTLR start "rule__Greeting__Group__6__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:538:1: rule__Greeting__Group__6__Impl : ( 'signaler' ) ;
    public final void rule__Greeting__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:542:1: ( ( 'signaler' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:543:1: ( 'signaler' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:543:1: ( 'signaler' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:544:1: 'signaler'
            {
             before(grammarAccess.getGreetingAccess().getSignalerKeyword_6()); 
            match(input,19,FOLLOW_19_in_rule__Greeting__Group__6__Impl1083); 
             after(grammarAccess.getGreetingAccess().getSignalerKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__6__Impl"


    // $ANTLR start "rule__Greeting__Group__7"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:557:1: rule__Greeting__Group__7 : rule__Greeting__Group__7__Impl rule__Greeting__Group__8 ;
    public final void rule__Greeting__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:561:1: ( rule__Greeting__Group__7__Impl rule__Greeting__Group__8 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:562:2: rule__Greeting__Group__7__Impl rule__Greeting__Group__8
            {
            pushFollow(FOLLOW_rule__Greeting__Group__7__Impl_in_rule__Greeting__Group__71114);
            rule__Greeting__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__8_in_rule__Greeting__Group__71117);
            rule__Greeting__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__7"


    // $ANTLR start "rule__Greeting__Group__7__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:569:1: rule__Greeting__Group__7__Impl : ( '=' ) ;
    public final void rule__Greeting__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:573:1: ( ( '=' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:574:1: ( '=' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:574:1: ( '=' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:575:1: '='
            {
             before(grammarAccess.getGreetingAccess().getEqualsSignKeyword_7()); 
            match(input,17,FOLLOW_17_in_rule__Greeting__Group__7__Impl1145); 
             after(grammarAccess.getGreetingAccess().getEqualsSignKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__7__Impl"


    // $ANTLR start "rule__Greeting__Group__8"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:588:1: rule__Greeting__Group__8 : rule__Greeting__Group__8__Impl rule__Greeting__Group__9 ;
    public final void rule__Greeting__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:592:1: ( rule__Greeting__Group__8__Impl rule__Greeting__Group__9 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:593:2: rule__Greeting__Group__8__Impl rule__Greeting__Group__9
            {
            pushFollow(FOLLOW_rule__Greeting__Group__8__Impl_in_rule__Greeting__Group__81176);
            rule__Greeting__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__9_in_rule__Greeting__Group__81179);
            rule__Greeting__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__8"


    // $ANTLR start "rule__Greeting__Group__8__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:600:1: rule__Greeting__Group__8__Impl : ( ( rule__Greeting__SignalerAssignment_8 ) ) ;
    public final void rule__Greeting__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:604:1: ( ( ( rule__Greeting__SignalerAssignment_8 ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:605:1: ( ( rule__Greeting__SignalerAssignment_8 ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:605:1: ( ( rule__Greeting__SignalerAssignment_8 ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:606:1: ( rule__Greeting__SignalerAssignment_8 )
            {
             before(grammarAccess.getGreetingAccess().getSignalerAssignment_8()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:607:1: ( rule__Greeting__SignalerAssignment_8 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:607:2: rule__Greeting__SignalerAssignment_8
            {
            pushFollow(FOLLOW_rule__Greeting__SignalerAssignment_8_in_rule__Greeting__Group__8__Impl1206);
            rule__Greeting__SignalerAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getGreetingAccess().getSignalerAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__8__Impl"


    // $ANTLR start "rule__Greeting__Group__9"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:617:1: rule__Greeting__Group__9 : rule__Greeting__Group__9__Impl rule__Greeting__Group__10 ;
    public final void rule__Greeting__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:621:1: ( rule__Greeting__Group__9__Impl rule__Greeting__Group__10 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:622:2: rule__Greeting__Group__9__Impl rule__Greeting__Group__10
            {
            pushFollow(FOLLOW_rule__Greeting__Group__9__Impl_in_rule__Greeting__Group__91236);
            rule__Greeting__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__10_in_rule__Greeting__Group__91239);
            rule__Greeting__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__9"


    // $ANTLR start "rule__Greeting__Group__9__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:629:1: rule__Greeting__Group__9__Impl : ( ',' ) ;
    public final void rule__Greeting__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:633:1: ( ( ',' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:634:1: ( ',' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:634:1: ( ',' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:635:1: ','
            {
             before(grammarAccess.getGreetingAccess().getCommaKeyword_9()); 
            match(input,18,FOLLOW_18_in_rule__Greeting__Group__9__Impl1267); 
             after(grammarAccess.getGreetingAccess().getCommaKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__9__Impl"


    // $ANTLR start "rule__Greeting__Group__10"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:648:1: rule__Greeting__Group__10 : rule__Greeting__Group__10__Impl rule__Greeting__Group__11 ;
    public final void rule__Greeting__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:652:1: ( rule__Greeting__Group__10__Impl rule__Greeting__Group__11 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:653:2: rule__Greeting__Group__10__Impl rule__Greeting__Group__11
            {
            pushFollow(FOLLOW_rule__Greeting__Group__10__Impl_in_rule__Greeting__Group__101298);
            rule__Greeting__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__11_in_rule__Greeting__Group__101301);
            rule__Greeting__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__10"


    // $ANTLR start "rule__Greeting__Group__10__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:660:1: rule__Greeting__Group__10__Impl : ( 'exceptionHandler' ) ;
    public final void rule__Greeting__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:664:1: ( ( 'exceptionHandler' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:665:1: ( 'exceptionHandler' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:665:1: ( 'exceptionHandler' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:666:1: 'exceptionHandler'
            {
             before(grammarAccess.getGreetingAccess().getExceptionHandlerKeyword_10()); 
            match(input,20,FOLLOW_20_in_rule__Greeting__Group__10__Impl1329); 
             after(grammarAccess.getGreetingAccess().getExceptionHandlerKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__10__Impl"


    // $ANTLR start "rule__Greeting__Group__11"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:679:1: rule__Greeting__Group__11 : rule__Greeting__Group__11__Impl rule__Greeting__Group__12 ;
    public final void rule__Greeting__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:683:1: ( rule__Greeting__Group__11__Impl rule__Greeting__Group__12 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:684:2: rule__Greeting__Group__11__Impl rule__Greeting__Group__12
            {
            pushFollow(FOLLOW_rule__Greeting__Group__11__Impl_in_rule__Greeting__Group__111360);
            rule__Greeting__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__12_in_rule__Greeting__Group__111363);
            rule__Greeting__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__11"


    // $ANTLR start "rule__Greeting__Group__11__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:691:1: rule__Greeting__Group__11__Impl : ( '=' ) ;
    public final void rule__Greeting__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:695:1: ( ( '=' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:696:1: ( '=' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:696:1: ( '=' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:697:1: '='
            {
             before(grammarAccess.getGreetingAccess().getEqualsSignKeyword_11()); 
            match(input,17,FOLLOW_17_in_rule__Greeting__Group__11__Impl1391); 
             after(grammarAccess.getGreetingAccess().getEqualsSignKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__11__Impl"


    // $ANTLR start "rule__Greeting__Group__12"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:710:1: rule__Greeting__Group__12 : rule__Greeting__Group__12__Impl rule__Greeting__Group__13 ;
    public final void rule__Greeting__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:714:1: ( rule__Greeting__Group__12__Impl rule__Greeting__Group__13 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:715:2: rule__Greeting__Group__12__Impl rule__Greeting__Group__13
            {
            pushFollow(FOLLOW_rule__Greeting__Group__12__Impl_in_rule__Greeting__Group__121422);
            rule__Greeting__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__13_in_rule__Greeting__Group__121425);
            rule__Greeting__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__12"


    // $ANTLR start "rule__Greeting__Group__12__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:722:1: rule__Greeting__Group__12__Impl : ( '{' ) ;
    public final void rule__Greeting__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:726:1: ( ( '{' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:727:1: ( '{' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:727:1: ( '{' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:728:1: '{'
            {
             before(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_12()); 
            match(input,15,FOLLOW_15_in_rule__Greeting__Group__12__Impl1453); 
             after(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__12__Impl"


    // $ANTLR start "rule__Greeting__Group__13"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:741:1: rule__Greeting__Group__13 : rule__Greeting__Group__13__Impl rule__Greeting__Group__14 ;
    public final void rule__Greeting__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:745:1: ( rule__Greeting__Group__13__Impl rule__Greeting__Group__14 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:746:2: rule__Greeting__Group__13__Impl rule__Greeting__Group__14
            {
            pushFollow(FOLLOW_rule__Greeting__Group__13__Impl_in_rule__Greeting__Group__131484);
            rule__Greeting__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__14_in_rule__Greeting__Group__131487);
            rule__Greeting__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__13"


    // $ANTLR start "rule__Greeting__Group__13__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:753:1: rule__Greeting__Group__13__Impl : ( ( rule__Greeting__ExceptionHandlersAssignment_13 )* ) ;
    public final void rule__Greeting__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:757:1: ( ( ( rule__Greeting__ExceptionHandlersAssignment_13 )* ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:758:1: ( ( rule__Greeting__ExceptionHandlersAssignment_13 )* )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:758:1: ( ( rule__Greeting__ExceptionHandlersAssignment_13 )* )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:759:1: ( rule__Greeting__ExceptionHandlersAssignment_13 )*
            {
             before(grammarAccess.getGreetingAccess().getExceptionHandlersAssignment_13()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:760:1: ( rule__Greeting__ExceptionHandlersAssignment_13 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==23) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:760:2: rule__Greeting__ExceptionHandlersAssignment_13
            	    {
            	    pushFollow(FOLLOW_rule__Greeting__ExceptionHandlersAssignment_13_in_rule__Greeting__Group__13__Impl1514);
            	    rule__Greeting__ExceptionHandlersAssignment_13();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getGreetingAccess().getExceptionHandlersAssignment_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__13__Impl"


    // $ANTLR start "rule__Greeting__Group__14"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:770:1: rule__Greeting__Group__14 : rule__Greeting__Group__14__Impl rule__Greeting__Group__15 ;
    public final void rule__Greeting__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:774:1: ( rule__Greeting__Group__14__Impl rule__Greeting__Group__15 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:775:2: rule__Greeting__Group__14__Impl rule__Greeting__Group__15
            {
            pushFollow(FOLLOW_rule__Greeting__Group__14__Impl_in_rule__Greeting__Group__141545);
            rule__Greeting__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__15_in_rule__Greeting__Group__141548);
            rule__Greeting__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__14"


    // $ANTLR start "rule__Greeting__Group__14__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:782:1: rule__Greeting__Group__14__Impl : ( '}' ) ;
    public final void rule__Greeting__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:786:1: ( ( '}' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:787:1: ( '}' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:787:1: ( '}' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:788:1: '}'
            {
             before(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_14()); 
            match(input,21,FOLLOW_21_in_rule__Greeting__Group__14__Impl1576); 
             after(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__14__Impl"


    // $ANTLR start "rule__Greeting__Group__15"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:801:1: rule__Greeting__Group__15 : rule__Greeting__Group__15__Impl rule__Greeting__Group__16 ;
    public final void rule__Greeting__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:805:1: ( rule__Greeting__Group__15__Impl rule__Greeting__Group__16 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:806:2: rule__Greeting__Group__15__Impl rule__Greeting__Group__16
            {
            pushFollow(FOLLOW_rule__Greeting__Group__15__Impl_in_rule__Greeting__Group__151607);
            rule__Greeting__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Greeting__Group__16_in_rule__Greeting__Group__151610);
            rule__Greeting__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__15"


    // $ANTLR start "rule__Greeting__Group__15__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:813:1: rule__Greeting__Group__15__Impl : ( '}' ) ;
    public final void rule__Greeting__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:817:1: ( ( '}' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:818:1: ( '}' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:818:1: ( '}' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:819:1: '}'
            {
             before(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_15()); 
            match(input,21,FOLLOW_21_in_rule__Greeting__Group__15__Impl1638); 
             after(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__15__Impl"


    // $ANTLR start "rule__Greeting__Group__16"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:832:1: rule__Greeting__Group__16 : rule__Greeting__Group__16__Impl ;
    public final void rule__Greeting__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:836:1: ( rule__Greeting__Group__16__Impl )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:837:2: rule__Greeting__Group__16__Impl
            {
            pushFollow(FOLLOW_rule__Greeting__Group__16__Impl_in_rule__Greeting__Group__161669);
            rule__Greeting__Group__16__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__16"


    // $ANTLR start "rule__Greeting__Group__16__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:843:1: rule__Greeting__Group__16__Impl : ( ';' ) ;
    public final void rule__Greeting__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:847:1: ( ( ';' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:848:1: ( ';' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:848:1: ( ';' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:849:1: ';'
            {
             before(grammarAccess.getGreetingAccess().getSemicolonKeyword_16()); 
            match(input,22,FOLLOW_22_in_rule__Greeting__Group__16__Impl1697); 
             after(grammarAccess.getGreetingAccess().getSemicolonKeyword_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__Group__16__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__0"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:896:1: rule__ExceptionHandler__Group__0 : rule__ExceptionHandler__Group__0__Impl rule__ExceptionHandler__Group__1 ;
    public final void rule__ExceptionHandler__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:900:1: ( rule__ExceptionHandler__Group__0__Impl rule__ExceptionHandler__Group__1 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:901:2: rule__ExceptionHandler__Group__0__Impl rule__ExceptionHandler__Group__1
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__0__Impl_in_rule__ExceptionHandler__Group__01762);
            rule__ExceptionHandler__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__1_in_rule__ExceptionHandler__Group__01765);
            rule__ExceptionHandler__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__0"


    // $ANTLR start "rule__ExceptionHandler__Group__0__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:908:1: rule__ExceptionHandler__Group__0__Impl : ( '[' ) ;
    public final void rule__ExceptionHandler__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:912:1: ( ( '[' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:913:1: ( '[' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:913:1: ( '[' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:914:1: '['
            {
             before(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_0()); 
            match(input,23,FOLLOW_23_in_rule__ExceptionHandler__Group__0__Impl1793); 
             after(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__0__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__1"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:927:1: rule__ExceptionHandler__Group__1 : rule__ExceptionHandler__Group__1__Impl rule__ExceptionHandler__Group__2 ;
    public final void rule__ExceptionHandler__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:931:1: ( rule__ExceptionHandler__Group__1__Impl rule__ExceptionHandler__Group__2 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:932:2: rule__ExceptionHandler__Group__1__Impl rule__ExceptionHandler__Group__2
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__1__Impl_in_rule__ExceptionHandler__Group__11824);
            rule__ExceptionHandler__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__2_in_rule__ExceptionHandler__Group__11827);
            rule__ExceptionHandler__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__1"


    // $ANTLR start "rule__ExceptionHandler__Group__1__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:939:1: rule__ExceptionHandler__Group__1__Impl : ( 'exception' ) ;
    public final void rule__ExceptionHandler__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:943:1: ( ( 'exception' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:944:1: ( 'exception' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:944:1: ( 'exception' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:945:1: 'exception'
            {
             before(grammarAccess.getExceptionHandlerAccess().getExceptionKeyword_1()); 
            match(input,24,FOLLOW_24_in_rule__ExceptionHandler__Group__1__Impl1855); 
             after(grammarAccess.getExceptionHandlerAccess().getExceptionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__1__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__2"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:958:1: rule__ExceptionHandler__Group__2 : rule__ExceptionHandler__Group__2__Impl rule__ExceptionHandler__Group__3 ;
    public final void rule__ExceptionHandler__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:962:1: ( rule__ExceptionHandler__Group__2__Impl rule__ExceptionHandler__Group__3 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:963:2: rule__ExceptionHandler__Group__2__Impl rule__ExceptionHandler__Group__3
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__2__Impl_in_rule__ExceptionHandler__Group__21886);
            rule__ExceptionHandler__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__3_in_rule__ExceptionHandler__Group__21889);
            rule__ExceptionHandler__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__2"


    // $ANTLR start "rule__ExceptionHandler__Group__2__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:970:1: rule__ExceptionHandler__Group__2__Impl : ( '=' ) ;
    public final void rule__ExceptionHandler__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:974:1: ( ( '=' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:975:1: ( '=' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:975:1: ( '=' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:976:1: '='
            {
             before(grammarAccess.getExceptionHandlerAccess().getEqualsSignKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__ExceptionHandler__Group__2__Impl1917); 
             after(grammarAccess.getExceptionHandlerAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__2__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__3"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:989:1: rule__ExceptionHandler__Group__3 : rule__ExceptionHandler__Group__3__Impl rule__ExceptionHandler__Group__4 ;
    public final void rule__ExceptionHandler__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:993:1: ( rule__ExceptionHandler__Group__3__Impl rule__ExceptionHandler__Group__4 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:994:2: rule__ExceptionHandler__Group__3__Impl rule__ExceptionHandler__Group__4
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__3__Impl_in_rule__ExceptionHandler__Group__31948);
            rule__ExceptionHandler__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__4_in_rule__ExceptionHandler__Group__31951);
            rule__ExceptionHandler__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__3"


    // $ANTLR start "rule__ExceptionHandler__Group__3__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1001:1: rule__ExceptionHandler__Group__3__Impl : ( ( rule__ExceptionHandler__ExceptionAssignment_3 ) ) ;
    public final void rule__ExceptionHandler__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1005:1: ( ( ( rule__ExceptionHandler__ExceptionAssignment_3 ) ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1006:1: ( ( rule__ExceptionHandler__ExceptionAssignment_3 ) )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1006:1: ( ( rule__ExceptionHandler__ExceptionAssignment_3 ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1007:1: ( rule__ExceptionHandler__ExceptionAssignment_3 )
            {
             before(grammarAccess.getExceptionHandlerAccess().getExceptionAssignment_3()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1008:1: ( rule__ExceptionHandler__ExceptionAssignment_3 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1008:2: rule__ExceptionHandler__ExceptionAssignment_3
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__ExceptionAssignment_3_in_rule__ExceptionHandler__Group__3__Impl1978);
            rule__ExceptionHandler__ExceptionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getExceptionHandlerAccess().getExceptionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__3__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__4"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1018:1: rule__ExceptionHandler__Group__4 : rule__ExceptionHandler__Group__4__Impl rule__ExceptionHandler__Group__5 ;
    public final void rule__ExceptionHandler__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1022:1: ( rule__ExceptionHandler__Group__4__Impl rule__ExceptionHandler__Group__5 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1023:2: rule__ExceptionHandler__Group__4__Impl rule__ExceptionHandler__Group__5
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__4__Impl_in_rule__ExceptionHandler__Group__42008);
            rule__ExceptionHandler__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__5_in_rule__ExceptionHandler__Group__42011);
            rule__ExceptionHandler__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__4"


    // $ANTLR start "rule__ExceptionHandler__Group__4__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1030:1: rule__ExceptionHandler__Group__4__Impl : ( 'handler = ' ) ;
    public final void rule__ExceptionHandler__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1034:1: ( ( 'handler = ' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1035:1: ( 'handler = ' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1035:1: ( 'handler = ' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1036:1: 'handler = '
            {
             before(grammarAccess.getExceptionHandlerAccess().getHandlerKeyword_4()); 
            match(input,25,FOLLOW_25_in_rule__ExceptionHandler__Group__4__Impl2039); 
             after(grammarAccess.getExceptionHandlerAccess().getHandlerKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__4__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__5"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1049:1: rule__ExceptionHandler__Group__5 : rule__ExceptionHandler__Group__5__Impl rule__ExceptionHandler__Group__6 ;
    public final void rule__ExceptionHandler__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1053:1: ( rule__ExceptionHandler__Group__5__Impl rule__ExceptionHandler__Group__6 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1054:2: rule__ExceptionHandler__Group__5__Impl rule__ExceptionHandler__Group__6
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__5__Impl_in_rule__ExceptionHandler__Group__52070);
            rule__ExceptionHandler__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__6_in_rule__ExceptionHandler__Group__52073);
            rule__ExceptionHandler__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__5"


    // $ANTLR start "rule__ExceptionHandler__Group__5__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1061:1: rule__ExceptionHandler__Group__5__Impl : ( '[' ) ;
    public final void rule__ExceptionHandler__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1065:1: ( ( '[' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1066:1: ( '[' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1066:1: ( '[' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1067:1: '['
            {
             before(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_5()); 
            match(input,23,FOLLOW_23_in_rule__ExceptionHandler__Group__5__Impl2101); 
             after(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__5__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__6"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1080:1: rule__ExceptionHandler__Group__6 : rule__ExceptionHandler__Group__6__Impl rule__ExceptionHandler__Group__7 ;
    public final void rule__ExceptionHandler__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1084:1: ( rule__ExceptionHandler__Group__6__Impl rule__ExceptionHandler__Group__7 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1085:2: rule__ExceptionHandler__Group__6__Impl rule__ExceptionHandler__Group__7
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__6__Impl_in_rule__ExceptionHandler__Group__62132);
            rule__ExceptionHandler__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__7_in_rule__ExceptionHandler__Group__62135);
            rule__ExceptionHandler__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__6"


    // $ANTLR start "rule__ExceptionHandler__Group__6__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1092:1: rule__ExceptionHandler__Group__6__Impl : ( ( rule__ExceptionHandler__HandlersAssignment_6 )* ) ;
    public final void rule__ExceptionHandler__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1096:1: ( ( ( rule__ExceptionHandler__HandlersAssignment_6 )* ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1097:1: ( ( rule__ExceptionHandler__HandlersAssignment_6 )* )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1097:1: ( ( rule__ExceptionHandler__HandlersAssignment_6 )* )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1098:1: ( rule__ExceptionHandler__HandlersAssignment_6 )*
            {
             before(grammarAccess.getExceptionHandlerAccess().getHandlersAssignment_6()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1099:1: ( rule__ExceptionHandler__HandlersAssignment_6 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_ID) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1099:2: rule__ExceptionHandler__HandlersAssignment_6
            	    {
            	    pushFollow(FOLLOW_rule__ExceptionHandler__HandlersAssignment_6_in_rule__ExceptionHandler__Group__6__Impl2162);
            	    rule__ExceptionHandler__HandlersAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getExceptionHandlerAccess().getHandlersAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__6__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__7"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1109:1: rule__ExceptionHandler__Group__7 : rule__ExceptionHandler__Group__7__Impl rule__ExceptionHandler__Group__8 ;
    public final void rule__ExceptionHandler__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1113:1: ( rule__ExceptionHandler__Group__7__Impl rule__ExceptionHandler__Group__8 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1114:2: rule__ExceptionHandler__Group__7__Impl rule__ExceptionHandler__Group__8
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__7__Impl_in_rule__ExceptionHandler__Group__72193);
            rule__ExceptionHandler__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ExceptionHandler__Group__8_in_rule__ExceptionHandler__Group__72196);
            rule__ExceptionHandler__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__7"


    // $ANTLR start "rule__ExceptionHandler__Group__7__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1121:1: rule__ExceptionHandler__Group__7__Impl : ( ']' ) ;
    public final void rule__ExceptionHandler__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1125:1: ( ( ']' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1126:1: ( ']' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1126:1: ( ']' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1127:1: ']'
            {
             before(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_7()); 
            match(input,26,FOLLOW_26_in_rule__ExceptionHandler__Group__7__Impl2224); 
             after(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__7__Impl"


    // $ANTLR start "rule__ExceptionHandler__Group__8"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1140:1: rule__ExceptionHandler__Group__8 : rule__ExceptionHandler__Group__8__Impl ;
    public final void rule__ExceptionHandler__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1144:1: ( rule__ExceptionHandler__Group__8__Impl )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1145:2: rule__ExceptionHandler__Group__8__Impl
            {
            pushFollow(FOLLOW_rule__ExceptionHandler__Group__8__Impl_in_rule__ExceptionHandler__Group__82255);
            rule__ExceptionHandler__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__8"


    // $ANTLR start "rule__ExceptionHandler__Group__8__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1151:1: rule__ExceptionHandler__Group__8__Impl : ( ']' ) ;
    public final void rule__ExceptionHandler__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1155:1: ( ( ']' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1156:1: ( ']' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1156:1: ( ']' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1157:1: ']'
            {
             before(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_8()); 
            match(input,26,FOLLOW_26_in_rule__ExceptionHandler__Group__8__Impl2283); 
             after(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__Group__8__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1188:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1192:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1193:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__02332);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__02335);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1200:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1204:1: ( ( RULE_ID ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1205:1: ( RULE_ID )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1205:1: ( RULE_ID )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1206:1: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl2362); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1217:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1221:1: ( rule__QualifiedName__Group__1__Impl )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1222:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__12391);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1228:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1232:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1233:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1233:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1234:1: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1235:1: ( rule__QualifiedName__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==27) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1235:2: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl2418);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1249:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1253:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1254:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__02453);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__02456);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1261:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1265:1: ( ( '.' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1266:1: ( '.' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1266:1: ( '.' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1267:1: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,27,FOLLOW_27_in_rule__QualifiedName__Group_1__0__Impl2484); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1280:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1284:1: ( rule__QualifiedName__Group_1__1__Impl )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1285:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__12515);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1291:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1295:1: ( ( RULE_ID ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1296:1: ( RULE_ID )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1296:1: ( RULE_ID )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1297:1: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl2542); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_1__0"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1312:1: rule__QualifiedNameWithWildcard__Group_1__0 : rule__QualifiedNameWithWildcard__Group_1__0__Impl rule__QualifiedNameWithWildcard__Group_1__1 ;
    public final void rule__QualifiedNameWithWildcard__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1316:1: ( rule__QualifiedNameWithWildcard__Group_1__0__Impl rule__QualifiedNameWithWildcard__Group_1__1 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1317:2: rule__QualifiedNameWithWildcard__Group_1__0__Impl rule__QualifiedNameWithWildcard__Group_1__1
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_1__0__Impl_in_rule__QualifiedNameWithWildcard__Group_1__02575);
            rule__QualifiedNameWithWildcard__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_1__1_in_rule__QualifiedNameWithWildcard__Group_1__02578);
            rule__QualifiedNameWithWildcard__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_1__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_1__0__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1324:1: rule__QualifiedNameWithWildcard__Group_1__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1328:1: ( ( ruleQualifiedName ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1329:1: ( ruleQualifiedName )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1329:1: ( ruleQualifiedName )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1330:1: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group_1__0__Impl2605);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_1__1"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1341:1: rule__QualifiedNameWithWildcard__Group_1__1 : rule__QualifiedNameWithWildcard__Group_1__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1345:1: ( rule__QualifiedNameWithWildcard__Group_1__1__Impl )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1346:2: rule__QualifiedNameWithWildcard__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_1__1__Impl_in_rule__QualifiedNameWithWildcard__Group_1__12634);
            rule__QualifiedNameWithWildcard__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_1__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_1__1__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1352:1: rule__QualifiedNameWithWildcard__Group_1__1__Impl : ( '.*' ) ;
    public final void rule__QualifiedNameWithWildcard__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1356:1: ( ( '.*' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1357:1: ( '.*' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1357:1: ( '.*' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1358:1: '.*'
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1_1()); 
            match(input,28,FOLLOW_28_in_rule__QualifiedNameWithWildcard__Group_1__1__Impl2662); 
             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_1__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_2__0"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1375:1: rule__QualifiedNameWithWildcard__Group_2__0 : rule__QualifiedNameWithWildcard__Group_2__0__Impl rule__QualifiedNameWithWildcard__Group_2__1 ;
    public final void rule__QualifiedNameWithWildcard__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1379:1: ( rule__QualifiedNameWithWildcard__Group_2__0__Impl rule__QualifiedNameWithWildcard__Group_2__1 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1380:2: rule__QualifiedNameWithWildcard__Group_2__0__Impl rule__QualifiedNameWithWildcard__Group_2__1
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_2__0__Impl_in_rule__QualifiedNameWithWildcard__Group_2__02697);
            rule__QualifiedNameWithWildcard__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_2__1_in_rule__QualifiedNameWithWildcard__Group_2__02700);
            rule__QualifiedNameWithWildcard__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_2__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_2__0__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1387:1: rule__QualifiedNameWithWildcard__Group_2__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1391:1: ( ( ruleQualifiedName ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1392:1: ( ruleQualifiedName )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1392:1: ( ruleQualifiedName )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1393:1: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group_2__0__Impl2727);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_2__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_2__1"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1404:1: rule__QualifiedNameWithWildcard__Group_2__1 : rule__QualifiedNameWithWildcard__Group_2__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1408:1: ( rule__QualifiedNameWithWildcard__Group_2__1__Impl )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1409:2: rule__QualifiedNameWithWildcard__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_2__1__Impl_in_rule__QualifiedNameWithWildcard__Group_2__12756);
            rule__QualifiedNameWithWildcard__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_2__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_2__1__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1415:1: rule__QualifiedNameWithWildcard__Group_2__1__Impl : ( '(..)' ) ;
    public final void rule__QualifiedNameWithWildcard__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1419:1: ( ( '(..)' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1420:1: ( '(..)' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1420:1: ( '(..)' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1421:1: '(..)'
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getLeftParenthesisFullStopFullStopRightParenthesisKeyword_2_1()); 
            match(input,29,FOLLOW_29_in_rule__QualifiedNameWithWildcard__Group_2__1__Impl2784); 
             after(grammarAccess.getQualifiedNameWithWildcardAccess().getLeftParenthesisFullStopFullStopRightParenthesisKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_2__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_3__0"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1438:1: rule__QualifiedNameWithWildcard__Group_3__0 : rule__QualifiedNameWithWildcard__Group_3__0__Impl rule__QualifiedNameWithWildcard__Group_3__1 ;
    public final void rule__QualifiedNameWithWildcard__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1442:1: ( rule__QualifiedNameWithWildcard__Group_3__0__Impl rule__QualifiedNameWithWildcard__Group_3__1 )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1443:2: rule__QualifiedNameWithWildcard__Group_3__0__Impl rule__QualifiedNameWithWildcard__Group_3__1
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_3__0__Impl_in_rule__QualifiedNameWithWildcard__Group_3__02819);
            rule__QualifiedNameWithWildcard__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_3__1_in_rule__QualifiedNameWithWildcard__Group_3__02822);
            rule__QualifiedNameWithWildcard__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_3__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_3__0__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1450:1: rule__QualifiedNameWithWildcard__Group_3__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1454:1: ( ( ruleQualifiedName ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1455:1: ( ruleQualifiedName )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1455:1: ( ruleQualifiedName )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1456:1: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group_3__0__Impl2849);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_3__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_3__1"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1467:1: rule__QualifiedNameWithWildcard__Group_3__1 : rule__QualifiedNameWithWildcard__Group_3__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1471:1: ( rule__QualifiedNameWithWildcard__Group_3__1__Impl )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1472:2: rule__QualifiedNameWithWildcard__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group_3__1__Impl_in_rule__QualifiedNameWithWildcard__Group_3__12878);
            rule__QualifiedNameWithWildcard__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_3__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group_3__1__Impl"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1478:1: rule__QualifiedNameWithWildcard__Group_3__1__Impl : ( '+' ) ;
    public final void rule__QualifiedNameWithWildcard__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1482:1: ( ( '+' ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1483:1: ( '+' )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1483:1: ( '+' )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1484:1: '+'
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getPlusSignKeyword_3_1()); 
            match(input,30,FOLLOW_30_in_rule__QualifiedNameWithWildcard__Group_3__1__Impl2906); 
             after(grammarAccess.getQualifiedNameWithWildcardAccess().getPlusSignKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group_3__1__Impl"


    // $ANTLR start "rule__Model__GreetingsAssignment"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1502:1: rule__Model__GreetingsAssignment : ( ruleGreeting ) ;
    public final void rule__Model__GreetingsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1506:1: ( ( ruleGreeting ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1507:1: ( ruleGreeting )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1507:1: ( ruleGreeting )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1508:1: ruleGreeting
            {
             before(grammarAccess.getModelAccess().getGreetingsGreetingParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleGreeting_in_rule__Model__GreetingsAssignment2946);
            ruleGreeting();

            state._fsp--;

             after(grammarAccess.getModelAccess().getGreetingsGreetingParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__GreetingsAssignment"


    // $ANTLR start "rule__Greeting__TypeContractAssignment_4"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1517:1: rule__Greeting__TypeContractAssignment_4 : ( ruleTypeRule ) ;
    public final void rule__Greeting__TypeContractAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1521:1: ( ( ruleTypeRule ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1522:1: ( ruleTypeRule )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1522:1: ( ruleTypeRule )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1523:1: ruleTypeRule
            {
             before(grammarAccess.getGreetingAccess().getTypeContractTypeRuleParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleTypeRule_in_rule__Greeting__TypeContractAssignment_42977);
            ruleTypeRule();

            state._fsp--;

             after(grammarAccess.getGreetingAccess().getTypeContractTypeRuleParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__TypeContractAssignment_4"


    // $ANTLR start "rule__Greeting__SignalerAssignment_8"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1532:1: rule__Greeting__SignalerAssignment_8 : ( ruleQualifiedNameWithWildcardSignaler ) ;
    public final void rule__Greeting__SignalerAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1536:1: ( ( ruleQualifiedNameWithWildcardSignaler ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1537:1: ( ruleQualifiedNameWithWildcardSignaler )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1537:1: ( ruleQualifiedNameWithWildcardSignaler )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1538:1: ruleQualifiedNameWithWildcardSignaler
            {
             before(grammarAccess.getGreetingAccess().getSignalerQualifiedNameWithWildcardSignalerParserRuleCall_8_0()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_rule__Greeting__SignalerAssignment_83008);
            ruleQualifiedNameWithWildcardSignaler();

            state._fsp--;

             after(grammarAccess.getGreetingAccess().getSignalerQualifiedNameWithWildcardSignalerParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__SignalerAssignment_8"


    // $ANTLR start "rule__Greeting__ExceptionHandlersAssignment_13"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1547:1: rule__Greeting__ExceptionHandlersAssignment_13 : ( ruleExceptionHandler ) ;
    public final void rule__Greeting__ExceptionHandlersAssignment_13() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1551:1: ( ( ruleExceptionHandler ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1552:1: ( ruleExceptionHandler )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1552:1: ( ruleExceptionHandler )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1553:1: ruleExceptionHandler
            {
             before(grammarAccess.getGreetingAccess().getExceptionHandlersExceptionHandlerParserRuleCall_13_0()); 
            pushFollow(FOLLOW_ruleExceptionHandler_in_rule__Greeting__ExceptionHandlersAssignment_133039);
            ruleExceptionHandler();

            state._fsp--;

             after(grammarAccess.getGreetingAccess().getExceptionHandlersExceptionHandlerParserRuleCall_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greeting__ExceptionHandlersAssignment_13"


    // $ANTLR start "rule__ExceptionHandler__ExceptionAssignment_3"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1562:1: rule__ExceptionHandler__ExceptionAssignment_3 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__ExceptionHandler__ExceptionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1566:1: ( ( ruleQualifiedNameWithWildcard ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1567:1: ( ruleQualifiedNameWithWildcard )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1567:1: ( ruleQualifiedNameWithWildcard )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1568:1: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getExceptionHandlerAccess().getExceptionQualifiedNameWithWildcardParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_rule__ExceptionHandler__ExceptionAssignment_33070);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getExceptionHandlerAccess().getExceptionQualifiedNameWithWildcardParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__ExceptionAssignment_3"


    // $ANTLR start "rule__ExceptionHandler__HandlersAssignment_6"
    // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1577:1: rule__ExceptionHandler__HandlersAssignment_6 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__ExceptionHandler__HandlersAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1581:1: ( ( ruleQualifiedNameWithWildcard ) )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1582:1: ( ruleQualifiedNameWithWildcard )
            {
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1582:1: ( ruleQualifiedNameWithWildcard )
            // ../org.xtext.example.daeh.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDaeh.g:1583:1: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getExceptionHandlerAccess().getHandlersQualifiedNameWithWildcardParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_rule__ExceptionHandler__HandlersAssignment_63101);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getExceptionHandlerAccess().getHandlersQualifiedNameWithWildcardParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExceptionHandler__HandlersAssignment_6"

    // Delegated rules


    protected DFA3 dfa3 = new DFA3(this);
    static final String DFA3_eotS =
        "\10\uffff";
    static final String DFA3_eofS =
        "\1\uffff\1\6\5\uffff\1\6";
    static final String DFA3_minS =
        "\3\4\4\uffff\1\4";
    static final String DFA3_maxS =
        "\1\4\1\36\1\4\4\uffff\1\36";
    static final String DFA3_acceptS =
        "\3\uffff\1\4\1\3\1\2\1\1\1\uffff";
    static final String DFA3_specialS =
        "\10\uffff}>";
    static final String[] DFA3_transitionS = {
            "\1\1",
            "\1\6\15\uffff\1\6\6\uffff\2\6\1\2\1\5\1\4\1\3",
            "\1\7",
            "",
            "",
            "",
            "",
            "\1\6\15\uffff\1\6\6\uffff\2\6\1\2\1\5\1\4\1\3"
    };

    static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
    static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
    static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
    static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
    static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
    static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
    static final short[][] DFA3_transition;

    static {
        int numStates = DFA3_transitionS.length;
        DFA3_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
        }
    }

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;
        }
        public String getDescription() {
            return "282:1: rule__QualifiedNameWithWildcard__Alternatives : ( ( ruleQualifiedName ) | ( ( rule__QualifiedNameWithWildcard__Group_1__0 ) ) | ( ( rule__QualifiedNameWithWildcard__Group_2__0 ) ) | ( ( rule__QualifiedNameWithWildcard__Group_3__0 ) ) );";
        }
    }
 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__GreetingsAssignment_in_ruleModel94 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_ruleGreeting_in_entryRuleGreeting122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGreeting129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__0_in_ruleGreeting155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExceptionHandler_in_entryRuleExceptionHandler182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExceptionHandler189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__0_in_ruleExceptionHandler215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeRule_in_entryRuleTypeRule242 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeRule249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TypeRule__Alternatives_in_ruleTypeRule275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard362 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Alternatives_in_ruleQualifiedNameWithWildcard395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_entryRuleQualifiedNameWithWildcardSignaler422 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcardSignaler429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcardSignaler__Alternatives_in_ruleQualifiedNameWithWildcardSignaler455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__TypeRule__Alternatives492 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__TypeRule__Alternatives512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Alternatives546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_1__0_in_rule__QualifiedNameWithWildcard__Alternatives563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_2__0_in_rule__QualifiedNameWithWildcard__Alternatives581 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_3__0_in_rule__QualifiedNameWithWildcard__Alternatives599 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_rule__QualifiedNameWithWildcardSignaler__Alternatives632 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__QualifiedNameWithWildcardSignaler__Alternatives650 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__0__Impl_in_rule__Greeting__Group__0682 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__1_in_rule__Greeting__Group__0685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Greeting__Group__0__Impl713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__1__Impl_in_rule__Greeting__Group__1744 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__2_in_rule__Greeting__Group__1747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Greeting__Group__1__Impl775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__2__Impl_in_rule__Greeting__Group__2806 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__3_in_rule__Greeting__Group__2809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Greeting__Group__2__Impl837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__3__Impl_in_rule__Greeting__Group__3868 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_rule__Greeting__Group__4_in_rule__Greeting__Group__3871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Greeting__Group__3__Impl899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__4__Impl_in_rule__Greeting__Group__4930 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__5_in_rule__Greeting__Group__4933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__TypeContractAssignment_4_in_rule__Greeting__Group__4__Impl960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__5__Impl_in_rule__Greeting__Group__5990 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__6_in_rule__Greeting__Group__5993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Greeting__Group__5__Impl1021 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__6__Impl_in_rule__Greeting__Group__61052 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__7_in_rule__Greeting__Group__61055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Greeting__Group__6__Impl1083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__7__Impl_in_rule__Greeting__Group__71114 = new BitSet(new long[]{0x0000000000002010L});
    public static final BitSet FOLLOW_rule__Greeting__Group__8_in_rule__Greeting__Group__71117 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Greeting__Group__7__Impl1145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__8__Impl_in_rule__Greeting__Group__81176 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__9_in_rule__Greeting__Group__81179 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__SignalerAssignment_8_in_rule__Greeting__Group__8__Impl1206 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__9__Impl_in_rule__Greeting__Group__91236 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__10_in_rule__Greeting__Group__91239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Greeting__Group__9__Impl1267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__10__Impl_in_rule__Greeting__Group__101298 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__11_in_rule__Greeting__Group__101301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Greeting__Group__10__Impl1329 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__11__Impl_in_rule__Greeting__Group__111360 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__12_in_rule__Greeting__Group__111363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Greeting__Group__11__Impl1391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__12__Impl_in_rule__Greeting__Group__121422 = new BitSet(new long[]{0x0000000000A00000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__13_in_rule__Greeting__Group__121425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Greeting__Group__12__Impl1453 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__13__Impl_in_rule__Greeting__Group__131484 = new BitSet(new long[]{0x0000000000A00000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__14_in_rule__Greeting__Group__131487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__ExceptionHandlersAssignment_13_in_rule__Greeting__Group__13__Impl1514 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__14__Impl_in_rule__Greeting__Group__141545 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__15_in_rule__Greeting__Group__141548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Greeting__Group__14__Impl1576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__15__Impl_in_rule__Greeting__Group__151607 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Greeting__Group__16_in_rule__Greeting__Group__151610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Greeting__Group__15__Impl1638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Greeting__Group__16__Impl_in_rule__Greeting__Group__161669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Greeting__Group__16__Impl1697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__0__Impl_in_rule__ExceptionHandler__Group__01762 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__1_in_rule__ExceptionHandler__Group__01765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__ExceptionHandler__Group__0__Impl1793 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__1__Impl_in_rule__ExceptionHandler__Group__11824 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__2_in_rule__ExceptionHandler__Group__11827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__ExceptionHandler__Group__1__Impl1855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__2__Impl_in_rule__ExceptionHandler__Group__21886 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__3_in_rule__ExceptionHandler__Group__21889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__ExceptionHandler__Group__2__Impl1917 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__3__Impl_in_rule__ExceptionHandler__Group__31948 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__4_in_rule__ExceptionHandler__Group__31951 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__ExceptionAssignment_3_in_rule__ExceptionHandler__Group__3__Impl1978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__4__Impl_in_rule__ExceptionHandler__Group__42008 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__5_in_rule__ExceptionHandler__Group__42011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__ExceptionHandler__Group__4__Impl2039 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__5__Impl_in_rule__ExceptionHandler__Group__52070 = new BitSet(new long[]{0x0000000004000010L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__6_in_rule__ExceptionHandler__Group__52073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__ExceptionHandler__Group__5__Impl2101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__6__Impl_in_rule__ExceptionHandler__Group__62132 = new BitSet(new long[]{0x0000000004000010L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__7_in_rule__ExceptionHandler__Group__62135 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__HandlersAssignment_6_in_rule__ExceptionHandler__Group__6__Impl2162 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__7__Impl_in_rule__ExceptionHandler__Group__72193 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__8_in_rule__ExceptionHandler__Group__72196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__ExceptionHandler__Group__7__Impl2224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ExceptionHandler__Group__8__Impl_in_rule__ExceptionHandler__Group__82255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__ExceptionHandler__Group__8__Impl2283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__02332 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__02335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl2362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__12391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl2418 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__02453 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__02456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__QualifiedName__Group_1__0__Impl2484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__12515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl2542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_1__0__Impl_in_rule__QualifiedNameWithWildcard__Group_1__02575 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_1__1_in_rule__QualifiedNameWithWildcard__Group_1__02578 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group_1__0__Impl2605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_1__1__Impl_in_rule__QualifiedNameWithWildcard__Group_1__12634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__QualifiedNameWithWildcard__Group_1__1__Impl2662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_2__0__Impl_in_rule__QualifiedNameWithWildcard__Group_2__02697 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_2__1_in_rule__QualifiedNameWithWildcard__Group_2__02700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group_2__0__Impl2727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_2__1__Impl_in_rule__QualifiedNameWithWildcard__Group_2__12756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__QualifiedNameWithWildcard__Group_2__1__Impl2784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_3__0__Impl_in_rule__QualifiedNameWithWildcard__Group_3__02819 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_3__1_in_rule__QualifiedNameWithWildcard__Group_3__02822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group_3__0__Impl2849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group_3__1__Impl_in_rule__QualifiedNameWithWildcard__Group_3__12878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__QualifiedNameWithWildcard__Group_3__1__Impl2906 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGreeting_in_rule__Model__GreetingsAssignment2946 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeRule_in_rule__Greeting__TypeContractAssignment_42977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_rule__Greeting__SignalerAssignment_83008 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExceptionHandler_in_rule__Greeting__ExceptionHandlersAssignment_133039 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_rule__ExceptionHandler__ExceptionAssignment_33070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_rule__ExceptionHandler__HandlersAssignment_63101 = new BitSet(new long[]{0x0000000000000002L});

}