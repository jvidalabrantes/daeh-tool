/*
* generated by Xtext
*/
grammar InternalDaeh;

options {
	superClass=AbstractInternalContentAssistParser;
	
}

@lexer::header {
package org.xtext.example.mydsl.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.DaehGrammarAccess;

}

@parser::members {
 
 	private DaehGrammarAccess grammarAccess;
 	
    public void setGrammarAccess(DaehGrammarAccess grammarAccess) {
    	this.grammarAccess = grammarAccess;
    }
    
    @Override
    protected Grammar getGrammar() {
    	return grammarAccess.getGrammar();
    }
    
    @Override
    protected String getValueForTokenName(String tokenName) {
    	return tokenName;
    }

}




// Entry rule entryRuleModel
entryRuleModel 
:
{ before(grammarAccess.getModelRule()); }
	 ruleModel
{ after(grammarAccess.getModelRule()); } 
	 EOF 
;

// Rule Model
ruleModel
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getModelAccess().getGreetingsAssignment()); }
(rule__Model__GreetingsAssignment)*
{ after(grammarAccess.getModelAccess().getGreetingsAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleGreeting
entryRuleGreeting 
:
{ before(grammarAccess.getGreetingRule()); }
	 ruleGreeting
{ after(grammarAccess.getGreetingRule()); } 
	 EOF 
;

// Rule Greeting
ruleGreeting
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getGreetingAccess().getGroup()); }
(rule__Greeting__Group__0)
{ after(grammarAccess.getGreetingAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExceptionHandler
entryRuleExceptionHandler 
:
{ before(grammarAccess.getExceptionHandlerRule()); }
	 ruleExceptionHandler
{ after(grammarAccess.getExceptionHandlerRule()); } 
	 EOF 
;

// Rule ExceptionHandler
ruleExceptionHandler
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExceptionHandlerAccess().getGroup()); }
(rule__ExceptionHandler__Group__0)
{ after(grammarAccess.getExceptionHandlerAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTypeRule
entryRuleTypeRule 
:
{ before(grammarAccess.getTypeRuleRule()); }
	 ruleTypeRule
{ after(grammarAccess.getTypeRuleRule()); } 
	 EOF 
;

// Rule TypeRule
ruleTypeRule
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTypeRuleAccess().getAlternatives()); }
(rule__TypeRule__Alternatives)
{ after(grammarAccess.getTypeRuleAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleQualifiedName
entryRuleQualifiedName 
:
{ before(grammarAccess.getQualifiedNameRule()); }
	 ruleQualifiedName
{ after(grammarAccess.getQualifiedNameRule()); } 
	 EOF 
;

// Rule QualifiedName
ruleQualifiedName
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getQualifiedNameAccess().getGroup()); }
(rule__QualifiedName__Group__0)
{ after(grammarAccess.getQualifiedNameAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleQualifiedNameWithWildcard
entryRuleQualifiedNameWithWildcard 
:
{ before(grammarAccess.getQualifiedNameWithWildcardRule()); }
	 ruleQualifiedNameWithWildcard
{ after(grammarAccess.getQualifiedNameWithWildcardRule()); } 
	 EOF 
;

// Rule QualifiedNameWithWildcard
ruleQualifiedNameWithWildcard
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getAlternatives()); }
(rule__QualifiedNameWithWildcard__Alternatives)
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleQualifiedNameWithWildcardSignaler
entryRuleQualifiedNameWithWildcardSignaler 
:
{ before(grammarAccess.getQualifiedNameWithWildcardSignalerRule()); }
	 ruleQualifiedNameWithWildcardSignaler
{ after(grammarAccess.getQualifiedNameWithWildcardSignalerRule()); } 
	 EOF 
;

// Rule QualifiedNameWithWildcardSignaler
ruleQualifiedNameWithWildcardSignaler
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAlternatives()); }
(rule__QualifiedNameWithWildcardSignaler__Alternatives)
{ after(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}




rule__TypeRule__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypeRuleAccess().getFullKeyword_0()); }

	'full' 

{ after(grammarAccess.getTypeRuleAccess().getFullKeyword_0()); }
)

    |(
{ before(grammarAccess.getTypeRuleAccess().getPartialKeyword_1()); }

	'partial' 

{ after(grammarAccess.getTypeRuleAccess().getPartialKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcard__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); }
	ruleQualifiedName
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_1()); }
(rule__QualifiedNameWithWildcard__Group_1__0)
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_1()); }
)

    |(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_2()); }
(rule__QualifiedNameWithWildcard__Group_2__0)
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_2()); }
)

    |(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_3()); }
(rule__QualifiedNameWithWildcard__Group_3__0)
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcardSignaler__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getQualifiedNameWithWildcardParserRuleCall_0()); }
	ruleQualifiedNameWithWildcard
{ after(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getQualifiedNameWithWildcardParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAsteriskKeyword_1()); }

	'*' 

{ after(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAsteriskKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}



rule__Greeting__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__0__Impl
	rule__Greeting__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getEhruleKeyword_0()); }

	'ehrule' 

{ after(grammarAccess.getGreetingAccess().getEhruleKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__1__Impl
	rule__Greeting__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_1()); }

	'{' 

{ after(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__2__Impl
	rule__Greeting__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getTypeKeyword_2()); }

	'type' 

{ after(grammarAccess.getGreetingAccess().getTypeKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__3__Impl
	rule__Greeting__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getEqualsSignKeyword_3()); }

	'=' 

{ after(grammarAccess.getGreetingAccess().getEqualsSignKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__4__Impl
	rule__Greeting__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getTypeContractAssignment_4()); }
(rule__Greeting__TypeContractAssignment_4)
{ after(grammarAccess.getGreetingAccess().getTypeContractAssignment_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__5__Impl
	rule__Greeting__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getCommaKeyword_5()); }

	',' 

{ after(grammarAccess.getGreetingAccess().getCommaKeyword_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__6__Impl
	rule__Greeting__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getSignalerKeyword_6()); }

	'signaler' 

{ after(grammarAccess.getGreetingAccess().getSignalerKeyword_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__7
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__7__Impl
	rule__Greeting__Group__8
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__7__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getEqualsSignKeyword_7()); }

	'=' 

{ after(grammarAccess.getGreetingAccess().getEqualsSignKeyword_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__8
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__8__Impl
	rule__Greeting__Group__9
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__8__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getSignalerAssignment_8()); }
(rule__Greeting__SignalerAssignment_8)
{ after(grammarAccess.getGreetingAccess().getSignalerAssignment_8()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__9
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__9__Impl
	rule__Greeting__Group__10
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__9__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getCommaKeyword_9()); }

	',' 

{ after(grammarAccess.getGreetingAccess().getCommaKeyword_9()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__10
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__10__Impl
	rule__Greeting__Group__11
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__10__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getExceptionHandlerKeyword_10()); }

	'exceptionHandler' 

{ after(grammarAccess.getGreetingAccess().getExceptionHandlerKeyword_10()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__11
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__11__Impl
	rule__Greeting__Group__12
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__11__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getEqualsSignKeyword_11()); }

	'=' 

{ after(grammarAccess.getGreetingAccess().getEqualsSignKeyword_11()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__12
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__12__Impl
	rule__Greeting__Group__13
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__12__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_12()); }

	'{' 

{ after(grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_12()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__13
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__13__Impl
	rule__Greeting__Group__14
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__13__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getExceptionHandlersAssignment_13()); }
(rule__Greeting__ExceptionHandlersAssignment_13)*
{ after(grammarAccess.getGreetingAccess().getExceptionHandlersAssignment_13()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__14
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__14__Impl
	rule__Greeting__Group__15
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__14__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_14()); }

	'}' 

{ after(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_14()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__15
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__15__Impl
	rule__Greeting__Group__16
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__15__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_15()); }

	'}' 

{ after(grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_15()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Greeting__Group__16
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Greeting__Group__16__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__Group__16__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getSemicolonKeyword_16()); }

	';' 

{ after(grammarAccess.getGreetingAccess().getSemicolonKeyword_16()); }
)

;
finally {
	restoreStackSize(stackSize);
}




































rule__ExceptionHandler__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__0__Impl
	rule__ExceptionHandler__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_0()); }

	'[' 

{ after(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__1__Impl
	rule__ExceptionHandler__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getExceptionKeyword_1()); }

	'exception' 

{ after(grammarAccess.getExceptionHandlerAccess().getExceptionKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__2__Impl
	rule__ExceptionHandler__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getEqualsSignKeyword_2()); }

	'=' 

{ after(grammarAccess.getExceptionHandlerAccess().getEqualsSignKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__3__Impl
	rule__ExceptionHandler__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getExceptionAssignment_3()); }
(rule__ExceptionHandler__ExceptionAssignment_3)
{ after(grammarAccess.getExceptionHandlerAccess().getExceptionAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__4__Impl
	rule__ExceptionHandler__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getHandlerKeyword_4()); }

	'handler = ' 

{ after(grammarAccess.getExceptionHandlerAccess().getHandlerKeyword_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__5__Impl
	rule__ExceptionHandler__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_5()); }

	'[' 

{ after(grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__6__Impl
	rule__ExceptionHandler__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getHandlersAssignment_6()); }
(rule__ExceptionHandler__HandlersAssignment_6)*
{ after(grammarAccess.getExceptionHandlerAccess().getHandlersAssignment_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__7
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__7__Impl
	rule__ExceptionHandler__Group__8
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__7__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_7()); }

	']' 

{ after(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExceptionHandler__Group__8
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExceptionHandler__Group__8__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__Group__8__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_8()); }

	']' 

{ after(grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_8()); }
)

;
finally {
	restoreStackSize(stackSize);
}




















rule__QualifiedName__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedName__Group__0__Impl
	rule__QualifiedName__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedName__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); }
	RULE_ID
{ after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__QualifiedName__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedName__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedName__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameAccess().getGroup_1()); }
(rule__QualifiedName__Group_1__0)*
{ after(grammarAccess.getQualifiedNameAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__QualifiedName__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedName__Group_1__0__Impl
	rule__QualifiedName__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedName__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); }

	'.' 

{ after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__QualifiedName__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedName__Group_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedName__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); }
	RULE_ID
{ after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__QualifiedNameWithWildcard__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedNameWithWildcard__Group_1__0__Impl
	rule__QualifiedNameWithWildcard__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcard__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_1_0()); }
	ruleQualifiedName
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__QualifiedNameWithWildcard__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedNameWithWildcard__Group_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcard__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1_1()); }

	'.*' 

{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__QualifiedNameWithWildcard__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedNameWithWildcard__Group_2__0__Impl
	rule__QualifiedNameWithWildcard__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcard__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_2_0()); }
	ruleQualifiedName
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__QualifiedNameWithWildcard__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedNameWithWildcard__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcard__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getLeftParenthesisFullStopFullStopRightParenthesisKeyword_2_1()); }

	'(..)' 

{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getLeftParenthesisFullStopFullStopRightParenthesisKeyword_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__QualifiedNameWithWildcard__Group_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedNameWithWildcard__Group_3__0__Impl
	rule__QualifiedNameWithWildcard__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcard__Group_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_3_0()); }
	ruleQualifiedName
{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__QualifiedNameWithWildcard__Group_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__QualifiedNameWithWildcard__Group_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__QualifiedNameWithWildcard__Group_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQualifiedNameWithWildcardAccess().getPlusSignKeyword_3_1()); }

	'+' 

{ after(grammarAccess.getQualifiedNameWithWildcardAccess().getPlusSignKeyword_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__Model__GreetingsAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getGreetingsGreetingParserRuleCall_0()); }
	ruleGreeting{ after(grammarAccess.getModelAccess().getGreetingsGreetingParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__TypeContractAssignment_4
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getTypeContractTypeRuleParserRuleCall_4_0()); }
	ruleTypeRule{ after(grammarAccess.getGreetingAccess().getTypeContractTypeRuleParserRuleCall_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__SignalerAssignment_8
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getSignalerQualifiedNameWithWildcardSignalerParserRuleCall_8_0()); }
	ruleQualifiedNameWithWildcardSignaler{ after(grammarAccess.getGreetingAccess().getSignalerQualifiedNameWithWildcardSignalerParserRuleCall_8_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Greeting__ExceptionHandlersAssignment_13
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getGreetingAccess().getExceptionHandlersExceptionHandlerParserRuleCall_13_0()); }
	ruleExceptionHandler{ after(grammarAccess.getGreetingAccess().getExceptionHandlersExceptionHandlerParserRuleCall_13_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__ExceptionAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getExceptionQualifiedNameWithWildcardParserRuleCall_3_0()); }
	ruleQualifiedNameWithWildcard{ after(grammarAccess.getExceptionHandlerAccess().getExceptionQualifiedNameWithWildcardParserRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExceptionHandler__HandlersAssignment_6
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExceptionHandlerAccess().getHandlersQualifiedNameWithWildcardParserRuleCall_6_0()); }
	ruleQualifiedNameWithWildcard{ after(grammarAccess.getExceptionHandlerAccess().getHandlersQualifiedNameWithWildcardParserRuleCall_6_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


