package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.DaehGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDaehParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'ehrule'", "'{'", "'type'", "'='", "','", "'signaler'", "'exceptionHandler'", "'}'", "';'", "'['", "'exception'", "'handler = '", "']'", "'full'", "'partial'", "'.'", "'.*'", "'(..)'", "'+'", "'*'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__30=30;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalDaehParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDaehParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDaehParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g"; }



     	private DaehGrammarAccess grammarAccess;
     	
        public InternalDaehParser(TokenStream input, DaehGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected DaehGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:67:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:68:2: (iv_ruleModel= ruleModel EOF )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:69:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel75);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:76:1: ruleModel returns [EObject current=null] : ( (lv_greetings_0_0= ruleGreeting ) )* ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_greetings_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:79:28: ( ( (lv_greetings_0_0= ruleGreeting ) )* )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:80:1: ( (lv_greetings_0_0= ruleGreeting ) )*
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:80:1: ( (lv_greetings_0_0= ruleGreeting ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:81:1: (lv_greetings_0_0= ruleGreeting )
            	    {
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:81:1: (lv_greetings_0_0= ruleGreeting )
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:82:3: lv_greetings_0_0= ruleGreeting
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getModelAccess().getGreetingsGreetingParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleGreeting_in_ruleModel130);
            	    lv_greetings_0_0=ruleGreeting();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"greetings",
            	            		lv_greetings_0_0, 
            	            		"Greeting");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleGreeting"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:106:1: entryRuleGreeting returns [EObject current=null] : iv_ruleGreeting= ruleGreeting EOF ;
    public final EObject entryRuleGreeting() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGreeting = null;


        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:107:2: (iv_ruleGreeting= ruleGreeting EOF )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:108:2: iv_ruleGreeting= ruleGreeting EOF
            {
             newCompositeNode(grammarAccess.getGreetingRule()); 
            pushFollow(FOLLOW_ruleGreeting_in_entryRuleGreeting166);
            iv_ruleGreeting=ruleGreeting();

            state._fsp--;

             current =iv_ruleGreeting; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGreeting176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGreeting"


    // $ANTLR start "ruleGreeting"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:115:1: ruleGreeting returns [EObject current=null] : (otherlv_0= 'ehrule' otherlv_1= '{' otherlv_2= 'type' otherlv_3= '=' ( (lv_typeContract_4_0= ruleTypeRule ) ) otherlv_5= ',' otherlv_6= 'signaler' otherlv_7= '=' ( (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler ) ) otherlv_9= ',' otherlv_10= 'exceptionHandler' otherlv_11= '=' otherlv_12= '{' ( (lv_exceptionHandlers_13_0= ruleExceptionHandler ) )* otherlv_14= '}' otherlv_15= '}' otherlv_16= ';' ) ;
    public final EObject ruleGreeting() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_typeContract_4_0 = null;

        AntlrDatatypeRuleToken lv_signaler_8_0 = null;

        EObject lv_exceptionHandlers_13_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:118:28: ( (otherlv_0= 'ehrule' otherlv_1= '{' otherlv_2= 'type' otherlv_3= '=' ( (lv_typeContract_4_0= ruleTypeRule ) ) otherlv_5= ',' otherlv_6= 'signaler' otherlv_7= '=' ( (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler ) ) otherlv_9= ',' otherlv_10= 'exceptionHandler' otherlv_11= '=' otherlv_12= '{' ( (lv_exceptionHandlers_13_0= ruleExceptionHandler ) )* otherlv_14= '}' otherlv_15= '}' otherlv_16= ';' ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:119:1: (otherlv_0= 'ehrule' otherlv_1= '{' otherlv_2= 'type' otherlv_3= '=' ( (lv_typeContract_4_0= ruleTypeRule ) ) otherlv_5= ',' otherlv_6= 'signaler' otherlv_7= '=' ( (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler ) ) otherlv_9= ',' otherlv_10= 'exceptionHandler' otherlv_11= '=' otherlv_12= '{' ( (lv_exceptionHandlers_13_0= ruleExceptionHandler ) )* otherlv_14= '}' otherlv_15= '}' otherlv_16= ';' )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:119:1: (otherlv_0= 'ehrule' otherlv_1= '{' otherlv_2= 'type' otherlv_3= '=' ( (lv_typeContract_4_0= ruleTypeRule ) ) otherlv_5= ',' otherlv_6= 'signaler' otherlv_7= '=' ( (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler ) ) otherlv_9= ',' otherlv_10= 'exceptionHandler' otherlv_11= '=' otherlv_12= '{' ( (lv_exceptionHandlers_13_0= ruleExceptionHandler ) )* otherlv_14= '}' otherlv_15= '}' otherlv_16= ';' )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:119:3: otherlv_0= 'ehrule' otherlv_1= '{' otherlv_2= 'type' otherlv_3= '=' ( (lv_typeContract_4_0= ruleTypeRule ) ) otherlv_5= ',' otherlv_6= 'signaler' otherlv_7= '=' ( (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler ) ) otherlv_9= ',' otherlv_10= 'exceptionHandler' otherlv_11= '=' otherlv_12= '{' ( (lv_exceptionHandlers_13_0= ruleExceptionHandler ) )* otherlv_14= '}' otherlv_15= '}' otherlv_16= ';'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleGreeting213); 

                	newLeafNode(otherlv_0, grammarAccess.getGreetingAccess().getEhruleKeyword_0());
                
            otherlv_1=(Token)match(input,12,FOLLOW_12_in_ruleGreeting225); 

                	newLeafNode(otherlv_1, grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_1());
                
            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleGreeting237); 

                	newLeafNode(otherlv_2, grammarAccess.getGreetingAccess().getTypeKeyword_2());
                
            otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleGreeting249); 

                	newLeafNode(otherlv_3, grammarAccess.getGreetingAccess().getEqualsSignKeyword_3());
                
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:135:1: ( (lv_typeContract_4_0= ruleTypeRule ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:136:1: (lv_typeContract_4_0= ruleTypeRule )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:136:1: (lv_typeContract_4_0= ruleTypeRule )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:137:3: lv_typeContract_4_0= ruleTypeRule
            {
             
            	        newCompositeNode(grammarAccess.getGreetingAccess().getTypeContractTypeRuleParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_ruleTypeRule_in_ruleGreeting270);
            lv_typeContract_4_0=ruleTypeRule();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGreetingRule());
            	        }
                   		set(
                   			current, 
                   			"typeContract",
                    		lv_typeContract_4_0, 
                    		"TypeRule");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,15,FOLLOW_15_in_ruleGreeting282); 

                	newLeafNode(otherlv_5, grammarAccess.getGreetingAccess().getCommaKeyword_5());
                
            otherlv_6=(Token)match(input,16,FOLLOW_16_in_ruleGreeting294); 

                	newLeafNode(otherlv_6, grammarAccess.getGreetingAccess().getSignalerKeyword_6());
                
            otherlv_7=(Token)match(input,14,FOLLOW_14_in_ruleGreeting306); 

                	newLeafNode(otherlv_7, grammarAccess.getGreetingAccess().getEqualsSignKeyword_7());
                
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:165:1: ( (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:166:1: (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:166:1: (lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:167:3: lv_signaler_8_0= ruleQualifiedNameWithWildcardSignaler
            {
             
            	        newCompositeNode(grammarAccess.getGreetingAccess().getSignalerQualifiedNameWithWildcardSignalerParserRuleCall_8_0()); 
            	    
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_ruleGreeting327);
            lv_signaler_8_0=ruleQualifiedNameWithWildcardSignaler();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGreetingRule());
            	        }
                   		set(
                   			current, 
                   			"signaler",
                    		lv_signaler_8_0, 
                    		"QualifiedNameWithWildcardSignaler");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_9=(Token)match(input,15,FOLLOW_15_in_ruleGreeting339); 

                	newLeafNode(otherlv_9, grammarAccess.getGreetingAccess().getCommaKeyword_9());
                
            otherlv_10=(Token)match(input,17,FOLLOW_17_in_ruleGreeting351); 

                	newLeafNode(otherlv_10, grammarAccess.getGreetingAccess().getExceptionHandlerKeyword_10());
                
            otherlv_11=(Token)match(input,14,FOLLOW_14_in_ruleGreeting363); 

                	newLeafNode(otherlv_11, grammarAccess.getGreetingAccess().getEqualsSignKeyword_11());
                
            otherlv_12=(Token)match(input,12,FOLLOW_12_in_ruleGreeting375); 

                	newLeafNode(otherlv_12, grammarAccess.getGreetingAccess().getLeftCurlyBracketKeyword_12());
                
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:199:1: ( (lv_exceptionHandlers_13_0= ruleExceptionHandler ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==20) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:200:1: (lv_exceptionHandlers_13_0= ruleExceptionHandler )
            	    {
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:200:1: (lv_exceptionHandlers_13_0= ruleExceptionHandler )
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:201:3: lv_exceptionHandlers_13_0= ruleExceptionHandler
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getGreetingAccess().getExceptionHandlersExceptionHandlerParserRuleCall_13_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleExceptionHandler_in_ruleGreeting396);
            	    lv_exceptionHandlers_13_0=ruleExceptionHandler();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getGreetingRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"exceptionHandlers",
            	            		lv_exceptionHandlers_13_0, 
            	            		"ExceptionHandler");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_14=(Token)match(input,18,FOLLOW_18_in_ruleGreeting409); 

                	newLeafNode(otherlv_14, grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_14());
                
            otherlv_15=(Token)match(input,18,FOLLOW_18_in_ruleGreeting421); 

                	newLeafNode(otherlv_15, grammarAccess.getGreetingAccess().getRightCurlyBracketKeyword_15());
                
            otherlv_16=(Token)match(input,19,FOLLOW_19_in_ruleGreeting433); 

                	newLeafNode(otherlv_16, grammarAccess.getGreetingAccess().getSemicolonKeyword_16());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGreeting"


    // $ANTLR start "entryRuleExceptionHandler"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:237:1: entryRuleExceptionHandler returns [EObject current=null] : iv_ruleExceptionHandler= ruleExceptionHandler EOF ;
    public final EObject entryRuleExceptionHandler() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExceptionHandler = null;


        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:238:2: (iv_ruleExceptionHandler= ruleExceptionHandler EOF )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:239:2: iv_ruleExceptionHandler= ruleExceptionHandler EOF
            {
             newCompositeNode(grammarAccess.getExceptionHandlerRule()); 
            pushFollow(FOLLOW_ruleExceptionHandler_in_entryRuleExceptionHandler469);
            iv_ruleExceptionHandler=ruleExceptionHandler();

            state._fsp--;

             current =iv_ruleExceptionHandler; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExceptionHandler479); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExceptionHandler"


    // $ANTLR start "ruleExceptionHandler"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:246:1: ruleExceptionHandler returns [EObject current=null] : (otherlv_0= '[' otherlv_1= 'exception' otherlv_2= '=' ( (lv_exception_3_0= ruleQualifiedNameWithWildcard ) ) otherlv_4= 'handler = ' otherlv_5= '[' ( (lv_handlers_6_0= ruleQualifiedNameWithWildcard ) )* otherlv_7= ']' otherlv_8= ']' ) ;
    public final EObject ruleExceptionHandler() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_exception_3_0 = null;

        AntlrDatatypeRuleToken lv_handlers_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:249:28: ( (otherlv_0= '[' otherlv_1= 'exception' otherlv_2= '=' ( (lv_exception_3_0= ruleQualifiedNameWithWildcard ) ) otherlv_4= 'handler = ' otherlv_5= '[' ( (lv_handlers_6_0= ruleQualifiedNameWithWildcard ) )* otherlv_7= ']' otherlv_8= ']' ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:250:1: (otherlv_0= '[' otherlv_1= 'exception' otherlv_2= '=' ( (lv_exception_3_0= ruleQualifiedNameWithWildcard ) ) otherlv_4= 'handler = ' otherlv_5= '[' ( (lv_handlers_6_0= ruleQualifiedNameWithWildcard ) )* otherlv_7= ']' otherlv_8= ']' )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:250:1: (otherlv_0= '[' otherlv_1= 'exception' otherlv_2= '=' ( (lv_exception_3_0= ruleQualifiedNameWithWildcard ) ) otherlv_4= 'handler = ' otherlv_5= '[' ( (lv_handlers_6_0= ruleQualifiedNameWithWildcard ) )* otherlv_7= ']' otherlv_8= ']' )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:250:3: otherlv_0= '[' otherlv_1= 'exception' otherlv_2= '=' ( (lv_exception_3_0= ruleQualifiedNameWithWildcard ) ) otherlv_4= 'handler = ' otherlv_5= '[' ( (lv_handlers_6_0= ruleQualifiedNameWithWildcard ) )* otherlv_7= ']' otherlv_8= ']'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleExceptionHandler516); 

                	newLeafNode(otherlv_0, grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_0());
                
            otherlv_1=(Token)match(input,21,FOLLOW_21_in_ruleExceptionHandler528); 

                	newLeafNode(otherlv_1, grammarAccess.getExceptionHandlerAccess().getExceptionKeyword_1());
                
            otherlv_2=(Token)match(input,14,FOLLOW_14_in_ruleExceptionHandler540); 

                	newLeafNode(otherlv_2, grammarAccess.getExceptionHandlerAccess().getEqualsSignKeyword_2());
                
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:262:1: ( (lv_exception_3_0= ruleQualifiedNameWithWildcard ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:263:1: (lv_exception_3_0= ruleQualifiedNameWithWildcard )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:263:1: (lv_exception_3_0= ruleQualifiedNameWithWildcard )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:264:3: lv_exception_3_0= ruleQualifiedNameWithWildcard
            {
             
            	        newCompositeNode(grammarAccess.getExceptionHandlerAccess().getExceptionQualifiedNameWithWildcardParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_ruleExceptionHandler561);
            lv_exception_3_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getExceptionHandlerRule());
            	        }
                   		set(
                   			current, 
                   			"exception",
                    		lv_exception_3_0, 
                    		"QualifiedNameWithWildcard");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,22,FOLLOW_22_in_ruleExceptionHandler573); 

                	newLeafNode(otherlv_4, grammarAccess.getExceptionHandlerAccess().getHandlerKeyword_4());
                
            otherlv_5=(Token)match(input,20,FOLLOW_20_in_ruleExceptionHandler585); 

                	newLeafNode(otherlv_5, grammarAccess.getExceptionHandlerAccess().getLeftSquareBracketKeyword_5());
                
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:288:1: ( (lv_handlers_6_0= ruleQualifiedNameWithWildcard ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:289:1: (lv_handlers_6_0= ruleQualifiedNameWithWildcard )
            	    {
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:289:1: (lv_handlers_6_0= ruleQualifiedNameWithWildcard )
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:290:3: lv_handlers_6_0= ruleQualifiedNameWithWildcard
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getExceptionHandlerAccess().getHandlersQualifiedNameWithWildcardParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_ruleExceptionHandler606);
            	    lv_handlers_6_0=ruleQualifiedNameWithWildcard();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getExceptionHandlerRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"handlers",
            	            		lv_handlers_6_0, 
            	            		"QualifiedNameWithWildcard");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_7=(Token)match(input,23,FOLLOW_23_in_ruleExceptionHandler619); 

                	newLeafNode(otherlv_7, grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_7());
                
            otherlv_8=(Token)match(input,23,FOLLOW_23_in_ruleExceptionHandler631); 

                	newLeafNode(otherlv_8, grammarAccess.getExceptionHandlerAccess().getRightSquareBracketKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExceptionHandler"


    // $ANTLR start "entryRuleTypeRule"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:322:1: entryRuleTypeRule returns [String current=null] : iv_ruleTypeRule= ruleTypeRule EOF ;
    public final String entryRuleTypeRule() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTypeRule = null;


        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:323:2: (iv_ruleTypeRule= ruleTypeRule EOF )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:324:2: iv_ruleTypeRule= ruleTypeRule EOF
            {
             newCompositeNode(grammarAccess.getTypeRuleRule()); 
            pushFollow(FOLLOW_ruleTypeRule_in_entryRuleTypeRule668);
            iv_ruleTypeRule=ruleTypeRule();

            state._fsp--;

             current =iv_ruleTypeRule.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeRule679); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeRule"


    // $ANTLR start "ruleTypeRule"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:331:1: ruleTypeRule returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'full' | kw= 'partial' ) ;
    public final AntlrDatatypeRuleToken ruleTypeRule() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:334:28: ( (kw= 'full' | kw= 'partial' ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:335:1: (kw= 'full' | kw= 'partial' )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:335:1: (kw= 'full' | kw= 'partial' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==24) ) {
                alt4=1;
            }
            else if ( (LA4_0==25) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:336:2: kw= 'full'
                    {
                    kw=(Token)match(input,24,FOLLOW_24_in_ruleTypeRule717); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTypeRuleAccess().getFullKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:343:2: kw= 'partial'
                    {
                    kw=(Token)match(input,25,FOLLOW_25_in_ruleTypeRule736); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTypeRuleAccess().getPartialKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeRule"


    // $ANTLR start "entryRuleQualifiedName"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:356:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:357:2: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:358:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName777);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName788); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:365:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:368:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:369:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:369:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:369:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualifiedName828); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
                
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:376:1: (kw= '.' this_ID_2= RULE_ID )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==26) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:377:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,26,FOLLOW_26_in_ruleQualifiedName847); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualifiedName862); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:397:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:398:2: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:399:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard910);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard921); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:406:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName | (this_QualifiedName_1= ruleQualifiedName kw= '.*' ) | (this_QualifiedName_3= ruleQualifiedName kw= '(..)' ) | (this_QualifiedName_5= ruleQualifiedName kw= '+' ) ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;

        AntlrDatatypeRuleToken this_QualifiedName_1 = null;

        AntlrDatatypeRuleToken this_QualifiedName_3 = null;

        AntlrDatatypeRuleToken this_QualifiedName_5 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:409:28: ( (this_QualifiedName_0= ruleQualifiedName | (this_QualifiedName_1= ruleQualifiedName kw= '.*' ) | (this_QualifiedName_3= ruleQualifiedName kw= '(..)' ) | (this_QualifiedName_5= ruleQualifiedName kw= '+' ) ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:410:1: (this_QualifiedName_0= ruleQualifiedName | (this_QualifiedName_1= ruleQualifiedName kw= '.*' ) | (this_QualifiedName_3= ruleQualifiedName kw= '(..)' ) | (this_QualifiedName_5= ruleQualifiedName kw= '+' ) )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:410:1: (this_QualifiedName_0= ruleQualifiedName | (this_QualifiedName_1= ruleQualifiedName kw= '.*' ) | (this_QualifiedName_3= ruleQualifiedName kw= '(..)' ) | (this_QualifiedName_5= ruleQualifiedName kw= '+' ) )
            int alt6=4;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:411:5: this_QualifiedName_0= ruleQualifiedName
                    {
                     
                            newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard968);
                    this_QualifiedName_0=ruleQualifiedName();

                    state._fsp--;


                    		current.merge(this_QualifiedName_0);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:422:6: (this_QualifiedName_1= ruleQualifiedName kw= '.*' )
                    {
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:422:6: (this_QualifiedName_1= ruleQualifiedName kw= '.*' )
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:423:5: this_QualifiedName_1= ruleQualifiedName kw= '.*'
                    {
                     
                            newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_1_0()); 
                        
                    pushFollow(FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard1002);
                    this_QualifiedName_1=ruleQualifiedName();

                    state._fsp--;


                    		current.merge(this_QualifiedName_1);
                        
                     
                            afterParserOrEnumRuleCall();
                        
                    kw=(Token)match(input,27,FOLLOW_27_in_ruleQualifiedNameWithWildcard1020); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:440:6: (this_QualifiedName_3= ruleQualifiedName kw= '(..)' )
                    {
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:440:6: (this_QualifiedName_3= ruleQualifiedName kw= '(..)' )
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:441:5: this_QualifiedName_3= ruleQualifiedName kw= '(..)'
                    {
                     
                            newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_2_0()); 
                        
                    pushFollow(FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard1050);
                    this_QualifiedName_3=ruleQualifiedName();

                    state._fsp--;


                    		current.merge(this_QualifiedName_3);
                        
                     
                            afterParserOrEnumRuleCall();
                        
                    kw=(Token)match(input,28,FOLLOW_28_in_ruleQualifiedNameWithWildcard1068); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getLeftParenthesisFullStopFullStopRightParenthesisKeyword_2_1()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:458:6: (this_QualifiedName_5= ruleQualifiedName kw= '+' )
                    {
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:458:6: (this_QualifiedName_5= ruleQualifiedName kw= '+' )
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:459:5: this_QualifiedName_5= ruleQualifiedName kw= '+'
                    {
                     
                            newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_3_0()); 
                        
                    pushFollow(FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard1098);
                    this_QualifiedName_5=ruleQualifiedName();

                    state._fsp--;


                    		current.merge(this_QualifiedName_5);
                        
                     
                            afterParserOrEnumRuleCall();
                        
                    kw=(Token)match(input,29,FOLLOW_29_in_ruleQualifiedNameWithWildcard1116); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getPlusSignKeyword_3_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedNameWithWildcardSignaler"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:483:1: entryRuleQualifiedNameWithWildcardSignaler returns [String current=null] : iv_ruleQualifiedNameWithWildcardSignaler= ruleQualifiedNameWithWildcardSignaler EOF ;
    public final String entryRuleQualifiedNameWithWildcardSignaler() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcardSignaler = null;


        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:484:2: (iv_ruleQualifiedNameWithWildcardSignaler= ruleQualifiedNameWithWildcardSignaler EOF )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:485:2: iv_ruleQualifiedNameWithWildcardSignaler= ruleQualifiedNameWithWildcardSignaler EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardSignalerRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_entryRuleQualifiedNameWithWildcardSignaler1158);
            iv_ruleQualifiedNameWithWildcardSignaler=ruleQualifiedNameWithWildcardSignaler();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcardSignaler.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcardSignaler1169); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcardSignaler"


    // $ANTLR start "ruleQualifiedNameWithWildcardSignaler"
    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:492:1: ruleQualifiedNameWithWildcardSignaler returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedNameWithWildcard_0= ruleQualifiedNameWithWildcard | kw= '*' ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcardSignaler() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedNameWithWildcard_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:495:28: ( (this_QualifiedNameWithWildcard_0= ruleQualifiedNameWithWildcard | kw= '*' ) )
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:496:1: (this_QualifiedNameWithWildcard_0= ruleQualifiedNameWithWildcard | kw= '*' )
            {
            // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:496:1: (this_QualifiedNameWithWildcard_0= ruleQualifiedNameWithWildcard | kw= '*' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==30) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:497:5: this_QualifiedNameWithWildcard_0= ruleQualifiedNameWithWildcard
                    {
                     
                            newCompositeNode(grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getQualifiedNameWithWildcardParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_ruleQualifiedNameWithWildcardSignaler1216);
                    this_QualifiedNameWithWildcard_0=ruleQualifiedNameWithWildcard();

                    state._fsp--;


                    		current.merge(this_QualifiedNameWithWildcard_0);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.daeh/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDaeh.g:509:2: kw= '*'
                    {
                    kw=(Token)match(input,30,FOLLOW_30_in_ruleQualifiedNameWithWildcardSignaler1240); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardSignalerAccess().getAsteriskKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcardSignaler"

    // Delegated rules


    protected DFA6 dfa6 = new DFA6(this);
    static final String DFA6_eotS =
        "\10\uffff";
    static final String DFA6_eofS =
        "\1\uffff\1\3\5\uffff\1\3";
    static final String DFA6_minS =
        "\3\4\4\uffff\1\4";
    static final String DFA6_maxS =
        "\1\4\1\35\1\4\4\uffff\1\35";
    static final String DFA6_acceptS =
        "\3\uffff\1\1\1\4\1\3\1\2\1\uffff";
    static final String DFA6_specialS =
        "\10\uffff}>";
    static final String[] DFA6_transitionS = {
            "\1\1",
            "\1\3\12\uffff\1\3\6\uffff\2\3\2\uffff\1\2\1\6\1\5\1\4",
            "\1\7",
            "",
            "",
            "",
            "",
            "\1\3\12\uffff\1\3\6\uffff\2\3\2\uffff\1\2\1\6\1\5\1\4"
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "410:1: (this_QualifiedName_0= ruleQualifiedName | (this_QualifiedName_1= ruleQualifiedName kw= '.*' ) | (this_QualifiedName_3= ruleQualifiedName kw= '(..)' ) | (this_QualifiedName_5= ruleQualifiedName kw= '+' ) )";
        }
    }
 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGreeting_in_ruleModel130 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_ruleGreeting_in_entryRuleGreeting166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGreeting176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleGreeting213 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleGreeting225 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleGreeting237 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleGreeting249 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_ruleTypeRule_in_ruleGreeting270 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleGreeting282 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleGreeting294 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleGreeting306 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_ruleGreeting327 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleGreeting339 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleGreeting351 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleGreeting363 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleGreeting375 = new BitSet(new long[]{0x0000000000140000L});
    public static final BitSet FOLLOW_ruleExceptionHandler_in_ruleGreeting396 = new BitSet(new long[]{0x0000000000140000L});
    public static final BitSet FOLLOW_18_in_ruleGreeting409 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleGreeting421 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleGreeting433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExceptionHandler_in_entryRuleExceptionHandler469 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExceptionHandler479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleExceptionHandler516 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleExceptionHandler528 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleExceptionHandler540 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_ruleExceptionHandler561 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleExceptionHandler573 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleExceptionHandler585 = new BitSet(new long[]{0x0000000000800010L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_ruleExceptionHandler606 = new BitSet(new long[]{0x0000000000800010L});
    public static final BitSet FOLLOW_23_in_ruleExceptionHandler619 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleExceptionHandler631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeRule_in_entryRuleTypeRule668 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeRule679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleTypeRule717 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleTypeRule736 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName777 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName828 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_26_in_ruleQualifiedName847 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName862 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard910 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard921 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard1002 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_ruleQualifiedNameWithWildcard1020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard1050 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_ruleQualifiedNameWithWildcard1068 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard1098 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleQualifiedNameWithWildcard1116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcardSignaler_in_entryRuleQualifiedNameWithWildcardSignaler1158 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcardSignaler1169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_ruleQualifiedNameWithWildcardSignaler1216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleQualifiedNameWithWildcardSignaler1240 = new BitSet(new long[]{0x0000000000000002L});

}