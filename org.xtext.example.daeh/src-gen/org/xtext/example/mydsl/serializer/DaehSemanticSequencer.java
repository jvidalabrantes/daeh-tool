package org.xtext.example.mydsl.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.xtext.example.mydsl.daeh.DaehPackage;
import org.xtext.example.mydsl.daeh.ExceptionHandler;
import org.xtext.example.mydsl.daeh.Greeting;
import org.xtext.example.mydsl.daeh.Model;
import org.xtext.example.mydsl.services.DaehGrammarAccess;

@SuppressWarnings("all")
public class DaehSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private DaehGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == DaehPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case DaehPackage.EXCEPTION_HANDLER:
				if(context == grammarAccess.getExceptionHandlerRule()) {
					sequence_ExceptionHandler(context, (ExceptionHandler) semanticObject); 
					return; 
				}
				else break;
			case DaehPackage.GREETING:
				if(context == grammarAccess.getGreetingRule()) {
					sequence_Greeting(context, (Greeting) semanticObject); 
					return; 
				}
				else break;
			case DaehPackage.MODEL:
				if(context == grammarAccess.getModelRule()) {
					sequence_Model(context, (Model) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (exception=QualifiedNameWithWildcard handlers+=QualifiedNameWithWildcard*)
	 */
	protected void sequence_ExceptionHandler(EObject context, ExceptionHandler semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (typeContract=TypeRule signaler=QualifiedNameWithWildcardSignaler exceptionHandlers+=ExceptionHandler*)
	 */
	protected void sequence_Greeting(EObject context, Greeting semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     greetings+=Greeting*
	 */
	protected void sequence_Model(EObject context, Model semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
