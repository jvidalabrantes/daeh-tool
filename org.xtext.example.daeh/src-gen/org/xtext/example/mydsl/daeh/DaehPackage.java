/**
 */
package org.xtext.example.mydsl.daeh;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.daeh.DaehFactory
 * @model kind="package"
 * @generated
 */
public interface DaehPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "daeh";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/example/mydsl/Daeh";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "daeh";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DaehPackage eINSTANCE = org.xtext.example.mydsl.daeh.impl.DaehPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.daeh.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.daeh.impl.ModelImpl
   * @see org.xtext.example.mydsl.daeh.impl.DaehPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Greetings</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__GREETINGS = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.daeh.impl.GreetingImpl <em>Greeting</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.daeh.impl.GreetingImpl
   * @see org.xtext.example.mydsl.daeh.impl.DaehPackageImpl#getGreeting()
   * @generated
   */
  int GREETING = 1;

  /**
   * The feature id for the '<em><b>Type Contract</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREETING__TYPE_CONTRACT = 0;

  /**
   * The feature id for the '<em><b>Signaler</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREETING__SIGNALER = 1;

  /**
   * The feature id for the '<em><b>Exception Handlers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREETING__EXCEPTION_HANDLERS = 2;

  /**
   * The number of structural features of the '<em>Greeting</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREETING_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.daeh.impl.ExceptionHandlerImpl <em>Exception Handler</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.daeh.impl.ExceptionHandlerImpl
   * @see org.xtext.example.mydsl.daeh.impl.DaehPackageImpl#getExceptionHandler()
   * @generated
   */
  int EXCEPTION_HANDLER = 2;

  /**
   * The feature id for the '<em><b>Exception</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPTION_HANDLER__EXCEPTION = 0;

  /**
   * The feature id for the '<em><b>Handlers</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPTION_HANDLER__HANDLERS = 1;

  /**
   * The number of structural features of the '<em>Exception Handler</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCEPTION_HANDLER_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.daeh.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see org.xtext.example.mydsl.daeh.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.daeh.Model#getGreetings <em>Greetings</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Greetings</em>'.
   * @see org.xtext.example.mydsl.daeh.Model#getGreetings()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Greetings();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.daeh.Greeting <em>Greeting</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Greeting</em>'.
   * @see org.xtext.example.mydsl.daeh.Greeting
   * @generated
   */
  EClass getGreeting();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.daeh.Greeting#getTypeContract <em>Type Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type Contract</em>'.
   * @see org.xtext.example.mydsl.daeh.Greeting#getTypeContract()
   * @see #getGreeting()
   * @generated
   */
  EAttribute getGreeting_TypeContract();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.daeh.Greeting#getSignaler <em>Signaler</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Signaler</em>'.
   * @see org.xtext.example.mydsl.daeh.Greeting#getSignaler()
   * @see #getGreeting()
   * @generated
   */
  EAttribute getGreeting_Signaler();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.daeh.Greeting#getExceptionHandlers <em>Exception Handlers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Exception Handlers</em>'.
   * @see org.xtext.example.mydsl.daeh.Greeting#getExceptionHandlers()
   * @see #getGreeting()
   * @generated
   */
  EReference getGreeting_ExceptionHandlers();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.daeh.ExceptionHandler <em>Exception Handler</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exception Handler</em>'.
   * @see org.xtext.example.mydsl.daeh.ExceptionHandler
   * @generated
   */
  EClass getExceptionHandler();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.daeh.ExceptionHandler#getException <em>Exception</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Exception</em>'.
   * @see org.xtext.example.mydsl.daeh.ExceptionHandler#getException()
   * @see #getExceptionHandler()
   * @generated
   */
  EAttribute getExceptionHandler_Exception();

  /**
   * Returns the meta object for the attribute list '{@link org.xtext.example.mydsl.daeh.ExceptionHandler#getHandlers <em>Handlers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Handlers</em>'.
   * @see org.xtext.example.mydsl.daeh.ExceptionHandler#getHandlers()
   * @see #getExceptionHandler()
   * @generated
   */
  EAttribute getExceptionHandler_Handlers();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  DaehFactory getDaehFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.daeh.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.daeh.impl.ModelImpl
     * @see org.xtext.example.mydsl.daeh.impl.DaehPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Greetings</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__GREETINGS = eINSTANCE.getModel_Greetings();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.daeh.impl.GreetingImpl <em>Greeting</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.daeh.impl.GreetingImpl
     * @see org.xtext.example.mydsl.daeh.impl.DaehPackageImpl#getGreeting()
     * @generated
     */
    EClass GREETING = eINSTANCE.getGreeting();

    /**
     * The meta object literal for the '<em><b>Type Contract</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GREETING__TYPE_CONTRACT = eINSTANCE.getGreeting_TypeContract();

    /**
     * The meta object literal for the '<em><b>Signaler</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GREETING__SIGNALER = eINSTANCE.getGreeting_Signaler();

    /**
     * The meta object literal for the '<em><b>Exception Handlers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GREETING__EXCEPTION_HANDLERS = eINSTANCE.getGreeting_ExceptionHandlers();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.daeh.impl.ExceptionHandlerImpl <em>Exception Handler</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.daeh.impl.ExceptionHandlerImpl
     * @see org.xtext.example.mydsl.daeh.impl.DaehPackageImpl#getExceptionHandler()
     * @generated
     */
    EClass EXCEPTION_HANDLER = eINSTANCE.getExceptionHandler();

    /**
     * The meta object literal for the '<em><b>Exception</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXCEPTION_HANDLER__EXCEPTION = eINSTANCE.getExceptionHandler_Exception();

    /**
     * The meta object literal for the '<em><b>Handlers</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXCEPTION_HANDLER__HANDLERS = eINSTANCE.getExceptionHandler_Handlers();

  }

} //DaehPackage
