/**
 */
package org.xtext.example.mydsl.daeh.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.xtext.example.mydsl.daeh.DaehPackage;
import org.xtext.example.mydsl.daeh.ExceptionHandler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exception Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.daeh.impl.ExceptionHandlerImpl#getException <em>Exception</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.daeh.impl.ExceptionHandlerImpl#getHandlers <em>Handlers</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExceptionHandlerImpl extends MinimalEObjectImpl.Container implements ExceptionHandler
{
  /**
   * The default value of the '{@link #getException() <em>Exception</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getException()
   * @generated
   * @ordered
   */
  protected static final String EXCEPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getException() <em>Exception</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getException()
   * @generated
   * @ordered
   */
  protected String exception = EXCEPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getHandlers() <em>Handlers</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHandlers()
   * @generated
   * @ordered
   */
  protected EList<String> handlers;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExceptionHandlerImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DaehPackage.Literals.EXCEPTION_HANDLER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getException()
  {
    return exception;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setException(String newException)
  {
    String oldException = exception;
    exception = newException;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DaehPackage.EXCEPTION_HANDLER__EXCEPTION, oldException, exception));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getHandlers()
  {
    if (handlers == null)
    {
      handlers = new EDataTypeEList<String>(String.class, this, DaehPackage.EXCEPTION_HANDLER__HANDLERS);
    }
    return handlers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DaehPackage.EXCEPTION_HANDLER__EXCEPTION:
        return getException();
      case DaehPackage.EXCEPTION_HANDLER__HANDLERS:
        return getHandlers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DaehPackage.EXCEPTION_HANDLER__EXCEPTION:
        setException((String)newValue);
        return;
      case DaehPackage.EXCEPTION_HANDLER__HANDLERS:
        getHandlers().clear();
        getHandlers().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DaehPackage.EXCEPTION_HANDLER__EXCEPTION:
        setException(EXCEPTION_EDEFAULT);
        return;
      case DaehPackage.EXCEPTION_HANDLER__HANDLERS:
        getHandlers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DaehPackage.EXCEPTION_HANDLER__EXCEPTION:
        return EXCEPTION_EDEFAULT == null ? exception != null : !EXCEPTION_EDEFAULT.equals(exception);
      case DaehPackage.EXCEPTION_HANDLER__HANDLERS:
        return handlers != null && !handlers.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (exception: ");
    result.append(exception);
    result.append(", handlers: ");
    result.append(handlers);
    result.append(')');
    return result.toString();
  }

} //ExceptionHandlerImpl
