/**
 */
package org.xtext.example.mydsl.daeh.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.daeh.DaehPackage;
import org.xtext.example.mydsl.daeh.ExceptionHandler;
import org.xtext.example.mydsl.daeh.Greeting;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Greeting</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.daeh.impl.GreetingImpl#getTypeContract <em>Type Contract</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.daeh.impl.GreetingImpl#getSignaler <em>Signaler</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.daeh.impl.GreetingImpl#getExceptionHandlers <em>Exception Handlers</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GreetingImpl extends MinimalEObjectImpl.Container implements Greeting
{
  /**
   * The default value of the '{@link #getTypeContract() <em>Type Contract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTypeContract()
   * @generated
   * @ordered
   */
  protected static final String TYPE_CONTRACT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTypeContract() <em>Type Contract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTypeContract()
   * @generated
   * @ordered
   */
  protected String typeContract = TYPE_CONTRACT_EDEFAULT;

  /**
   * The default value of the '{@link #getSignaler() <em>Signaler</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSignaler()
   * @generated
   * @ordered
   */
  protected static final String SIGNALER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSignaler() <em>Signaler</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSignaler()
   * @generated
   * @ordered
   */
  protected String signaler = SIGNALER_EDEFAULT;

  /**
   * The cached value of the '{@link #getExceptionHandlers() <em>Exception Handlers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExceptionHandlers()
   * @generated
   * @ordered
   */
  protected EList<ExceptionHandler> exceptionHandlers;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GreetingImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DaehPackage.Literals.GREETING;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTypeContract()
  {
    return typeContract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTypeContract(String newTypeContract)
  {
    String oldTypeContract = typeContract;
    typeContract = newTypeContract;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DaehPackage.GREETING__TYPE_CONTRACT, oldTypeContract, typeContract));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSignaler()
  {
    return signaler;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSignaler(String newSignaler)
  {
    String oldSignaler = signaler;
    signaler = newSignaler;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DaehPackage.GREETING__SIGNALER, oldSignaler, signaler));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ExceptionHandler> getExceptionHandlers()
  {
    if (exceptionHandlers == null)
    {
      exceptionHandlers = new EObjectContainmentEList<ExceptionHandler>(ExceptionHandler.class, this, DaehPackage.GREETING__EXCEPTION_HANDLERS);
    }
    return exceptionHandlers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DaehPackage.GREETING__EXCEPTION_HANDLERS:
        return ((InternalEList<?>)getExceptionHandlers()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DaehPackage.GREETING__TYPE_CONTRACT:
        return getTypeContract();
      case DaehPackage.GREETING__SIGNALER:
        return getSignaler();
      case DaehPackage.GREETING__EXCEPTION_HANDLERS:
        return getExceptionHandlers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DaehPackage.GREETING__TYPE_CONTRACT:
        setTypeContract((String)newValue);
        return;
      case DaehPackage.GREETING__SIGNALER:
        setSignaler((String)newValue);
        return;
      case DaehPackage.GREETING__EXCEPTION_HANDLERS:
        getExceptionHandlers().clear();
        getExceptionHandlers().addAll((Collection<? extends ExceptionHandler>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DaehPackage.GREETING__TYPE_CONTRACT:
        setTypeContract(TYPE_CONTRACT_EDEFAULT);
        return;
      case DaehPackage.GREETING__SIGNALER:
        setSignaler(SIGNALER_EDEFAULT);
        return;
      case DaehPackage.GREETING__EXCEPTION_HANDLERS:
        getExceptionHandlers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DaehPackage.GREETING__TYPE_CONTRACT:
        return TYPE_CONTRACT_EDEFAULT == null ? typeContract != null : !TYPE_CONTRACT_EDEFAULT.equals(typeContract);
      case DaehPackage.GREETING__SIGNALER:
        return SIGNALER_EDEFAULT == null ? signaler != null : !SIGNALER_EDEFAULT.equals(signaler);
      case DaehPackage.GREETING__EXCEPTION_HANDLERS:
        return exceptionHandlers != null && !exceptionHandlers.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (typeContract: ");
    result.append(typeContract);
    result.append(", signaler: ");
    result.append(signaler);
    result.append(')');
    return result.toString();
  }

} //GreetingImpl
