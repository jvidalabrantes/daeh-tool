/**
 */
package org.xtext.example.mydsl.daeh;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Greeting</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.daeh.Greeting#getTypeContract <em>Type Contract</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.daeh.Greeting#getSignaler <em>Signaler</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.daeh.Greeting#getExceptionHandlers <em>Exception Handlers</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.daeh.DaehPackage#getGreeting()
 * @model
 * @generated
 */
public interface Greeting extends EObject
{
  /**
   * Returns the value of the '<em><b>Type Contract</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type Contract</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type Contract</em>' attribute.
   * @see #setTypeContract(String)
   * @see org.xtext.example.mydsl.daeh.DaehPackage#getGreeting_TypeContract()
   * @model
   * @generated
   */
  String getTypeContract();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.daeh.Greeting#getTypeContract <em>Type Contract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type Contract</em>' attribute.
   * @see #getTypeContract()
   * @generated
   */
  void setTypeContract(String value);

  /**
   * Returns the value of the '<em><b>Signaler</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Signaler</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Signaler</em>' attribute.
   * @see #setSignaler(String)
   * @see org.xtext.example.mydsl.daeh.DaehPackage#getGreeting_Signaler()
   * @model
   * @generated
   */
  String getSignaler();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.daeh.Greeting#getSignaler <em>Signaler</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Signaler</em>' attribute.
   * @see #getSignaler()
   * @generated
   */
  void setSignaler(String value);

  /**
   * Returns the value of the '<em><b>Exception Handlers</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.daeh.ExceptionHandler}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exception Handlers</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exception Handlers</em>' containment reference list.
   * @see org.xtext.example.mydsl.daeh.DaehPackage#getGreeting_ExceptionHandlers()
   * @model containment="true"
   * @generated
   */
  EList<ExceptionHandler> getExceptionHandlers();

} // Greeting
