/**
 */
package org.xtext.example.mydsl.daeh;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exception Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.daeh.ExceptionHandler#getException <em>Exception</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.daeh.ExceptionHandler#getHandlers <em>Handlers</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.daeh.DaehPackage#getExceptionHandler()
 * @model
 * @generated
 */
public interface ExceptionHandler extends EObject
{
  /**
   * Returns the value of the '<em><b>Exception</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exception</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exception</em>' attribute.
   * @see #setException(String)
   * @see org.xtext.example.mydsl.daeh.DaehPackage#getExceptionHandler_Exception()
   * @model
   * @generated
   */
  String getException();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.daeh.ExceptionHandler#getException <em>Exception</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exception</em>' attribute.
   * @see #getException()
   * @generated
   */
  void setException(String value);

  /**
   * Returns the value of the '<em><b>Handlers</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Handlers</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Handlers</em>' attribute list.
   * @see org.xtext.example.mydsl.daeh.DaehPackage#getExceptionHandler_Handlers()
   * @model unique="false"
   * @generated
   */
  EList<String> getHandlers();

} // ExceptionHandler
