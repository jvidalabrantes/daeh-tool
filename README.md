This repository contains the DAEH tool which enables Java developers
to specify and dynamically monitor the exception handling policy of a
system.The DAEH tool is based on a domain-specific language to specify
the exception handling policy. It uses aspect oriented programming to
instrument the code and dynamically check such policy.

DAEH is designed in the form of an aspect library which performs
load-time instrumentation of the exception handling code.

This repository also contains all the open-source data used in DAEH
evaluation  described on the paper "DAEH: A Tool for Specifying and
Monitoring the Exception Handling Policy" submitted to International
Journal of Software Engineering and Knowledge Engineering. More
specifically: (i) all contracts defined; (ii) all data violations
collected; (iii) the JMeter test suite; (iv) the results of
performance testes; and (iv) the DAEH tool we developed to support
this work.