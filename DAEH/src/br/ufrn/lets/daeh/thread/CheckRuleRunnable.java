package br.ufrn.lets.daeh.thread;

import br.ufrn.lets.daeh.controller.ContractController;
import br.ufrn.lets.daeh.exceptions.ContractCheckingException;


public class CheckRuleRunnable implements Runnable {
	
	private org.aspectj.lang.Signature handlerSignature;
	
	private Throwable exception;
	
	public CheckRuleRunnable (org.aspectj.lang.Signature handlerSignature, Throwable exception) {
		this.handlerSignature = handlerSignature;
		this.exception = exception;
	}

	
	public void run() {
		try {
			ContractController.getInstance().verifyContract(exception,handlerSignature);
		} catch (ContractCheckingException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	
	
}
