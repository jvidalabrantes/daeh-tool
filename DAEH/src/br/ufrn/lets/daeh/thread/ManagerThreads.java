package br.ufrn.lets.daeh.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ManagerThreads {

	
	private ExecutorService executorService = Executors.newFixedThreadPool(50);
	
	
	public void createCheckRule (org.aspectj.lang.Signature handlerSignature, Throwable exception) {
		
		try {
			CheckRuleRunnable checkRuleRunnable = new CheckRuleRunnable(handlerSignature, exception);
			executorService.execute(checkRuleRunnable);
		} catch (Exception e) {
			System.out.println("Erro na criação de checkRule");
			System.out.println(e);
		}
	}
	
	
	private static class ManagerThreadsHolder {
		private static final ManagerThreads INSTANCE = new ManagerThreads();
	}
	
	public static ManagerThreads getInstance() {
		return ManagerThreadsHolder.INSTANCE;
	}
	
	
}
