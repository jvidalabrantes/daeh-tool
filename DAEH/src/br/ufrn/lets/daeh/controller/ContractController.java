package br.ufrn.lets.daeh.controller;


import java.io.File;

import org.aspectj.lang.Signature;

import br.ufrn.lets.daeh.entities.Contract;
import br.ufrn.lets.daeh.exceptions.ContractCheckingException;
import br.ufrn.lets.daeh.util.ContractUtilXML;


/**
 * Classe criada para servir como controller das operações
 * relacionadas ao contrato ({@link Contract})
 * @author Joilson Abrantes
 * @author Ricardo Sales
 *
 */
public class ContractController {
	
	
	//O contrato a ser manipulado
	private Contract contract;
	
	
	public ContractController(){

	}
	
	public void loadContracts(File fileName){
		contract = ContractUtilXML.loadContractByFile(fileName);
	}
	
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	/**
	 * Método para verificar se o contrato está sendo obedecido
	 * @param exception Exceção que foi lançada pelo programa
	 * @param handlerSignature Quem capturou a exceção
	 * @param font A fonte da verificação do contrato. Pode ser
	 * a interceptação da aplicação, do próprio framework, ou de uma classe de teste.
	 * @throws ContractCheckingException
	 * @throws Exception 
	 * @throws ClassNotFoundException 
	 */
	public void verifyContract(Throwable exception, Signature handlerSignature) throws ContractCheckingException, ClassNotFoundException{
		contract.verifyIfContractIsObeyed(exception,handlerSignature);
	}
	
	private static class ContractControllerHolder {
		private static final ContractController INSTANCE = new ContractController();
	}
	
	public static ContractController getInstance() {
		return ContractControllerHolder.INSTANCE;
	}
	
}
