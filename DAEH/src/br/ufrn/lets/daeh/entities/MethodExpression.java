package br.ufrn.lets.daeh.entities;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.lets.daeh.exceptions.MethodNotFoundException;

/**
 * Classe que representa uma expressão de um método.
 * OBS: Atualmente a expressão é formada pelo 
 * [CaminhoDaClasse].[NomeDaClasse].[NomeDoMetodo]
 * 
 * @author Ricardo Sales
 * @author Joilson Abrantes
 *
 */
public abstract class MethodExpression {
	
	private String expression;
	

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getExpression() {
		return expression;
	}
	
	
	/**
	 * A partir do {@link MethodExpression} retorna a String com
	 * o nome completo da classe (nome com o caminho do pacote).
	 * @return
	 */
	public String getCompleteClassNameFromExpression() {
		
		String nomeDoMetodoSemParametros = getStringUntilParenthesis(expression);
		int posicaoUltimoPonto = nomeDoMetodoSemParametros.lastIndexOf('.');
		return nomeDoMetodoSemParametros.substring(0, posicaoUltimoPonto);
	}
	
	/**
	 * A partir do {@link MethodExpression} retorna a String com
	 * o nome do método da classe.
	 * @param exp
	 * @return
	 */
	public String getMethodNameFromExpression(String expressao){
		String nomeDoMetodoSemParametros = getStringUntilParenthesis(expressao);
		int posicaoUltimoPonto = nomeDoMetodoSemParametros.lastIndexOf('.');
		return nomeDoMetodoSemParametros.substring(posicaoUltimoPonto + 1, nomeDoMetodoSemParametros.length());
	}

	
	private String getStringUntilParenthesis (String str){
		int primeiroParenteste = str.lastIndexOf('(');
		return str.substring(0,primeiroParenteste);
	}
	
	public boolean matchMethodSignature(String m) throws MethodNotFoundException, ClassNotFoundException {
		boolean status = false;
		if (matchMethodName(m)){
			if (matchParameters(m)) {
				status = true;
			}
		}
		return status;

	}
	
	private String getStringBetweenParenthesis (String str){
		int primeiroParenteste = str.lastIndexOf('(');
		int ultimoParentese = str.lastIndexOf(')');
		
		return str.substring(primeiroParenteste+1, ultimoParentese);
	}
	
	private boolean matchMethodName (String m) throws MethodNotFoundException{
		boolean status = false;
		String padrao = getMethodNameFromExpression(expression);
		String nomeSignature = getMethodNameFromExpression(m);
		int tamString = 0;
		String prefix = "";
		while(tamString < padrao.length()){
			char aux = padrao.charAt(tamString);
			if (aux == '*') {
				if (tamString == padrao.length()-1) {
					if (nomeSignature.startsWith(prefix)) {
						return true;
					} else {
						return false;
					}
				} else {
					throw new MethodNotFoundException("It was not possible to verify signaler expression. Please verify if there is an error in expression:" + getExpression());
				}
			} else {
				if (tamString == padrao.length()-1) {
					prefix+=aux;
					if (nomeSignature.equals(prefix)) {
						return true;
					} else {
						return false;
					}
				}  else {
					prefix += aux;
				}
			}
			tamString++;
		}
		return status;
	}
	
	private boolean matchParameters (String m) throws ClassNotFoundException{
		String parametrosExpression = getStringBetweenParenthesis(expression);
		String parametrosMethod = getStringBetweenParenthesis(m);
		if (parametrosExpression.equals("..")) {
			return true;
		} else {
			List<String> pExpression = getParametersFromString(parametrosExpression);
			List<String> pMethod = getParametersFromString(parametrosMethod);
			
			if (pExpression.size() != pMethod.size()) {
				return false;
			}
			
			for (int i = 0 ; i < pExpression.size() ; i++) {
				if (pExpression.get(i).equals("*")) {
					return true;
				} else if (!pExpression.get(i).equals((pMethod.get(i)))) {
					return false;
				}
			}
			return true;
		}
		
		
	}
	
	private List<String> getParametersFromString(String parameters) throws ClassNotFoundException{
		String parametros = parameters;
		ArrayList<String> pList = new ArrayList<String>();
		String umParametro = getStringUntilVirgula(parametros);
		while (!umParametro.equals("")){
			pList.add(umParametro);
			parametros = getStringAfterVirgula(parametros);
			umParametro = getStringUntilVirgula(parametros);
		}
		return pList;
	}

	private String getStringUntilVirgula(String parametros) {
		String aux = "";
		if (parametros.length() != 0) {
			if (parametros.indexOf(',') < 0) { 
				return parametros;
			} else {
				aux = parametros.substring(0, parametros.indexOf(','));
			}
		}
		return aux;
	}
	private String getStringAfterVirgula(String parametros) {
		String aux = "";
		if (parametros.length() != 0) {
			if (parametros.indexOf(',') < 0) {
				return aux;
			}
			aux = parametros.substring(parametros.indexOf(','),parametros.length());
		}
		return aux;
	}
	
}
