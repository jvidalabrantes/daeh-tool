package br.ufrn.lets.daeh.entities;

public enum ContractType {
	
	FULL, PARTIAL
}
