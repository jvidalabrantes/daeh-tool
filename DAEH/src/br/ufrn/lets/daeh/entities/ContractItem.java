package br.ufrn.lets.daeh.entities;


import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa um item do contrato
 * Um item do contrato é formado por 
 * um Signaler (Classe ou aspecto que dispara a exceção) e um {@link ExceptionTarget}
 * 
 * @author Joilson Abrantes 
 * @author Ricardo Sales
 *
 */
public class ContractItem {
	
	
	
	/**
	 * Objeto que representa o signaler (pode ser uma expressão 
	 * regular que define um conjunto de signalers)
	 */
	private Signaler signaler;
	
	/**
	 * Lista de exceções que podem ser lançadas pelo signaler
	 */
	private List<ExceptionTarget> exceptionsTargets;
	
	private ContractType contractType;
	
	/**
	 * Inicializa o objeto, instanciando a lista vazia
	 */
	public ContractItem(){
		exceptionsTargets = new ArrayList<ExceptionTarget>();
	}

	
	public Signaler getSignler() {
		return signaler;
	}
	
	
	public void setSignler(Signaler signaler) {
		this.signaler = signaler;
	}

	public List<ExceptionTarget> getExceptionsTargets() {
		return exceptionsTargets;
	}

	public void setExceptionsTargets(List<ExceptionTarget> exceptionsTargets) {
		this.exceptionsTargets = exceptionsTargets;
	}
	
	public ContractType getContractType() {
		return contractType;
	}


	public void setContractType(ContractType contractType) {
		this.contractType = contractType;
	}


	public void addExceptionTarget(ExceptionTarget et){
		this.exceptionsTargets.add(et);
	}
	
	public void addExceptionTarget(List<ExceptionTarget> exceptionTargets){
		
		for (ExceptionTarget exceptionTarget : exceptionTargets) {
			this.exceptionsTargets.add(exceptionTarget);
		}
		
	}
	
	/**
	 * M�todo que retorna a lista com os exception targets
	 * que tem determinada exce��o
	 * @param exception
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ExceptionTarget> findExceptionTargetsByException(Class exception){
		List<ExceptionTarget> etargets = new ArrayList<ExceptionTarget>();
		for (ExceptionTarget et : exceptionsTargets){
			
			if(et.getexceptionType().equals(exception) || (  et.isConsiderarSubClasses() && et.getexceptionType().isAssignableFrom(exception))){
				etargets.add(et);
			}
		}
		return etargets;
	}

}
