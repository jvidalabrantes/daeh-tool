package br.ufrn.lets.daeh.entities;

import br.ufrn.lets.daeh.exceptions.MethodNotFoundException;


/**
 * 
 * @author Joilson Abrantes
 *
 */
public class Signaler extends MethodExpression {
	
	public boolean contains (StackTraceElement stackTraceSignaler) throws MethodNotFoundException {
		
		boolean status = false;
		if (this.getExpression().equals("*")) {
			return true;
		}
		
		if (!this.getExpression().endsWith("*")) {
			return verificaMetodoExiste(stackTraceSignaler);
		} else {
			String pacote = this.getExpression().replace(".*", "");
			if (stackTraceSignaler.getClassName().startsWith(pacote)){
				return true;
			}
		}
		return status;
	}
	
	/**
	 * Verifica se o contrato tem algum método definido no singler que esteja no stack trace.
	 * @param stackTraceSignaler
	 * @return
	 */
	private boolean verificaMetodoExiste(StackTraceElement stackTraceSignaler) {
		String padrao = getMethodNameFromExpression(this.getExpression());
		int tamString = 0;
		String prefix = "";
		while(tamString < padrao.length()) {
			char aux = padrao.charAt(tamString);
			if (aux == '*') {
				if (tamString == padrao.length()-1) {
					if (stackTraceSignaler.getMethodName().startsWith(prefix)) {
						return true;
					} else {
						return false;
					}
				}
				else {
					throw new MethodNotFoundException("It was not possible to verify signaler expression. Please verify if there is an error in expression:" + getExpression());
				}
			}
			else {
				if (tamString == padrao.length()-1) {
					prefix+=aux;
					if (stackTraceSignaler.getMethodName().equals(prefix)) {
						return true;
					} else {
						return false;
					}
				}
				else prefix += aux;
			}
			tamString++;
		}
		
		return false;
	}
}
