package br.ufrn.lets.daeh.entities;

import br.ufrn.lets.daeh.exceptions.ContractCheckingException;
import br.ufrn.lets.daeh.exceptions.MethodNotFoundException;

/**
 * 
 * 
 * @author Joilson Abrantes
 *
 */
public class Handler extends MethodExpression {
	
	@SuppressWarnings("rawtypes")
	public boolean contains (org.aspectj.lang.Signature assinatura) throws MethodNotFoundException, ClassNotFoundException {
		

		Class classAssinatura = assinatura.getDeclaringType();
		Class classExpressao;
		
		if ( getExpression().lastIndexOf(".*") ==  (getExpression().length() -2)){
			String inicioPacote = getExpression().substring(0, getExpression().lastIndexOf(".*"));
			if ( classAssinatura.getName().startsWith(inicioPacote)){
				return true;
			}else{
				return false;
			}
		}

		try {
			classExpressao = Class.forName(getCompleteClassNameFromExpression(), true, Thread.currentThread().getContextClassLoader());
			if (classAssinatura != classExpressao) return false;
		} catch (ClassNotFoundException e1) {
			throw new ContractCheckingException("Incorrect Checking: class " + getCompleteClassNameFromExpression() + 
					" from handler expression " + getExpression() + " does not exist");
		}
		
		
			if (matchMethodSignature(assinatura.toLongString())) {
				
				return true;
			}
			else return false;
	}
}	
