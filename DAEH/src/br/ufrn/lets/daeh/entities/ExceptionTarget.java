package br.ufrn.lets.daeh.entities;


import java.util.ArrayList;
import java.util.List;


/**
 * 
 * Representa o relacionamento entre uma exceção e seus Handler.
 * 
 * @author Joilson Abrantes
 *
 */
public class ExceptionTarget {

	/**
	 * O tipo da exceção
	 */
	@SuppressWarnings("rawtypes")
	private Class exceptionType;
	
	/**
	 * Lista de assinaturas dos handlers
	 * Cada handler é uma expressão regular
	 */
	private List<Handler> handlerSignatures;
	
	/**
	 * Indica se deve considerar as subclasses;
	 */
	private boolean considerarSubClasses;
	
	public ExceptionTarget() {
		
		handlerSignatures = new ArrayList<Handler>();
	}
	

	@SuppressWarnings("rawtypes")
	public Class getexceptionType() {
		return exceptionType;
	}
	
	@SuppressWarnings("rawtypes")
	public void setexceptionType(Class type) {
		this.exceptionType = type;
	}
	
	
	public List<Handler> getHandlerSignature() {
		return handlerSignatures;
	}
	
	
	public void setHandlerSignatures(List<Handler> handlerSignature) {
		this.handlerSignatures = handlerSignature;
	}
	
	
	public void addHandlerSignature(Handler handlerSignature){
		handlerSignatures.add(handlerSignature);
	}

	public boolean isConsiderarSubClasses() {
		return considerarSubClasses;
	}

	public void setConsiderarSubClasses(boolean considerarSubClasses) {
		this.considerarSubClasses = considerarSubClasses;
	}
	
}
