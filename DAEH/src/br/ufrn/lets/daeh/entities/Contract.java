package br.ufrn.lets.daeh.entities;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.Signature;

import br.ufrn.lets.daeh.comunication.ComunicationServer;
import br.ufrn.lets.daeh.exceptions.ContractCheckingException;
import br.ufrn.lets.daeh.exceptions.MethodNotFoundException;
import br.ufrn.lets.daeh.util.PropertiesDAEH;

/**
 * Classe que representa um contrato excepcional.
 * eh formada por varios {@link ContractItem} (que representa um item do contrato)
 * @author Joilson Abrantes
 * @author Ricardo Sales
 *
 */
public class Contract {
	
	/**
	 * Representa a lista de itens do contrato
	 */
	private List<ContractItem> contractItems;
		
	public Contract(){
		contractItems = new ArrayList<ContractItem>();
	}

	public List<ContractItem> getContractItem() {
		return contractItems;
	}

	public void setContractItem(List<ContractItem> contractItem) {
		this.contractItems = contractItem;
	}
	
	public void addContractItem (ContractItem ci){
		contractItems.add(ci);
	}
	
	/**
	 * Retorna os itens do contrato que possuem um determinado signaler
	 * @param signalerFromStackTrace nome do signaler
	 * @return 
	 * @throws Exception 
	 */
	public List<ContractItem> findContractItemsBySignlerFromStackTrace(StackTraceElement signalerFromStackTrace) throws MethodNotFoundException {
		List<ContractItem> cis = new ArrayList<ContractItem>();
		for (ContractItem ci : contractItems){
			if(ci.getSignler().contains(signalerFromStackTrace)){
				cis.add(ci);
			}
		}
		return cis;
	}
	
	/**
	 * Verifica se o contrato está sendo obedecido
	 *
	 * @param exception A exceção a ser checada
	 * @param handler Quem capturou a exceção
	 * @param isFromFramework Esse boleano identifica se a exceção foi capturada pelo framework ou nao.
	 * @throws ContractCheckingException
	 * @throws Exception 
	 * @throws ClassNotFoundException 
	 */
	public void verifyIfContractIsObeyed(Throwable exception, Signature handler) throws ContractCheckingException, MethodNotFoundException, ClassNotFoundException {

		for (StackTraceElement ste: exception.getStackTrace()){
			List<ContractItem> cis = null;
			cis = findContractItemsBySignlerFromStackTrace(ste);
			for (ContractItem ci : cis){
				List<ExceptionTarget> etl = ci.findExceptionTargetsByException(exception.getClass());
				if (ci.getContractType().equals(ContractType.FULL) && etl.isEmpty()) {
					String exceptionMessage = "\nError in Exception Handling contract: \n\t<Signaler:" + ci.getSignler().getExpression() + ">" +
							"\n It was not specified <Exception:" + exception.getClass().toString() +">";
					sendNotification(exception, exceptionMessage);
				}
				
				if (etl.size() > 0) {
					for (ExceptionTarget et : etl){
						boolean match = false;
						List<String> handlersMatched = new ArrayList<String>();
						for (Handler handlerSignatureFromContract : et.getHandlerSignature()){
							if(handlerSignatureFromContract.contains(handler)) {
								handlersMatched.add(handler.toLongString());
								match = true;
							}
						}
						if (!match && handler!=null){
							String exceptionMessage = "\nError in Exception Handling contract: \n\t<Signaler:" + ci.getSignler().getExpression() + "> \n\t<Exception:" + et.getexceptionType() + ">" +
							"\n expected: <Handlers:" + printSignatures(et.getHandlerSignature()) + "> \n but was <Handler:" + handler.toShortString() +">";
							sendNotification(exception, exceptionMessage);
							return;
						}
					}
				}
			}
		}
	}

	private void sendNotification(Throwable exception, String exceptionMessage) {
		boolean envioParaServidorHabilitado = PropertiesDAEH.getHabilitadoServidorDAEH();
		if (envioParaServidorHabilitado) {
			ComunicationServer comunicationServer = new ComunicationServer();
			comunicationServer.saveEvent(exception, exceptionMessage);
		} else {
			System.out.println(exceptionMessage);
		}
	}

	


	/**
	 * Método que retorna uma string com as assinaturas dos métodos
	 * de uma lista de {@link MethodExpression}s.
	 * @param handlerSignature
	 * @return
	 */
	private String printSignatures(List<Handler> handlerSignature) {
		String aux = "";
		for (MethodExpression me : handlerSignature){
			aux += "[" + me.getExpression() + "]";
			if (handlerSignature.indexOf(me) < handlerSignature.size()-1)
				aux += " or ";
		}
		return aux;
		
	}
		
		
}