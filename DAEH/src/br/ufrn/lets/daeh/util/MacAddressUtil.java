package br.ufrn.lets.daeh.util;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * 
 * Obtém o endereço MAC do host que está executando o cliente DAEH.
 * 
 * @author Joilson Abrantes
 *
 */
public class MacAddressUtil {
	
	public static String getMacAddress() throws UnknownHostException, SocketException {
//	    InetAddress localHost = InetAddress.getLocalHost();
//	    NetworkInterface netInter = NetworkInterface.getByInetAddress( localHost );
	    Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
	    while(networkInterfaces.hasMoreElements()){
	    	NetworkInterface network = networkInterfaces.nextElement();
	        byte[] bmac = network.getHardwareAddress();
	        if (bmac != null && bmac.length > 0) {
		        String macAddress =  String.format ( "%1$02x-%2$02x-%3$02x-%4$02x-%5$02x-%6$02x",
		        		bmac[0], bmac[1],
		        		bmac[2], bmac[3],
		        		bmac[4], bmac[5] ).toUpperCase();
		        
		        return macAddress;
	        }
	    }

	    return "NAO_IDENTIFICADO_MAC";
	  }

}
