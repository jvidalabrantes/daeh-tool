package br.ufrn.lets.daeh.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import br.ufrn.lets.daeh.entities.Contract;
import br.ufrn.lets.daeh.entities.ContractItem;
import br.ufrn.lets.daeh.entities.ContractType;
import br.ufrn.lets.daeh.entities.ExceptionTarget;
import br.ufrn.lets.daeh.entities.Handler;
import br.ufrn.lets.daeh.entities.Signaler;

public class ContractUtilXML {

	/**
	 * Carrega o contrato a partir do arquivo xml.
	 * @param file name of xml File
	 * @throws IOException 
	 * @throws JDOMException 
	 * @throws ClassNotFoundException
	 */
	public static Contract loadContractByFile(File file) {

		Contract contract = new Contract();
		try {
			SAXBuilder sAXBuilder = new SAXBuilder();  
			Document document = sAXBuilder.build(file);  
			Element root = document.getRootElement();
			List elements = root.getChildren();
			Iterator iterator = elements.iterator();
			while (iterator.hasNext()) { 
				Element element = (Element) iterator.next(); 
				
				Signaler signaler = new Signaler();
				signaler.setExpression(element.getAttributeValue("signaler").trim());
				String contractType = element.getAttributeValue("type").trim();
				List<ExceptionTarget> exceptionTargets = createExceptionTargets(
						iterator, element);
				
				ContractItem contractItem = new ContractItem();
				contractItem.setSignler(signaler);
				contractItem.setContractType(getContractType(contractType));
				contractItem.addExceptionTarget(exceptionTargets);
				contract.addContractItem(contractItem);
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contract;
	}
	
	/**
	 * 
	 * @param iterator
	 * @param element
	 * @return
	 */
	private static List<ExceptionTarget> createExceptionTargets(
			Iterator iterator, Element element) {
		List<ExceptionTarget> exceptionTargets = new ArrayList<ExceptionTarget>();
		List exceptionHandler = element.getChildren();
		Iterator iteratorExceptionHandler  = exceptionHandler.iterator();
		while (iteratorExceptionHandler.hasNext()) {
			Element elementExceptionHandler = (Element) iteratorExceptionHandler.next();
			ExceptionTarget exceptionTarget = new ExceptionTarget();
			String exceptionType = elementExceptionHandler.getAttributeValue("type").trim();
			if (exceptionType.endsWith("+")){
				exceptionTarget.setConsiderarSubClasses(true);
				exceptionType = exceptionType.replace("+", "");
			}
			try {
				Class classe = Class.forName(exceptionType, true, Thread.currentThread().getContextClassLoader());
				exceptionTarget.setexceptionType(classe);
			} catch (ClassNotFoundException e) {
				System.out.println("The exception " + exceptionType + " informed on contract File does not exist on Project");
			}
			List<Handler> handlers = createHandlers(elementExceptionHandler);
			exceptionTarget.setHandlerSignatures(handlers);
			exceptionTargets.add(exceptionTarget);
		}
		return exceptionTargets;
	}
	
	/**
	 * 
	 * @param elementExceptionHandler
	 * @return
	 */
	private static List<Handler> createHandlers(Element elementExceptionHandler) {
		List<Handler> handlers = new ArrayList<Handler>();
		List listhandler = elementExceptionHandler.getChildren();
		Iterator iteratorhandler  = listhandler.iterator();
		while (iteratorhandler.hasNext()) {
			Element elementHandler = (Element) iteratorhandler.next();
			String stringhandler = elementHandler.getAttributeValue("signature").trim();
			Handler handler = new Handler();
			handler.setExpression(stringhandler);
			handlers.add(handler);
		}
		return handlers;
	}
	
	/**
	 * Retorma o tipo do contrato.
	 * @param contractType
	 * @return
	 */
	private static ContractType getContractType(String contractType) {
		if (contractType.equals("partial")) {
			return ContractType.PARTIAL;
		}
		return ContractType.FULL;
	}


}
