package br.ufrn.lets.daeh.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 * 
 * Carrega as propriedades de configuração da ferramenta.
 * 
 * @author Joilson Abrantes
 *
 */
public class PropertiesUtil {

	private static Properties propriedades;
	private final String file = "config"+File.separatorChar+"configuration.properties";
	
	private PropertiesUtil() {
		propriedades = new Properties();
		loadSettings();	
	}
	
	@SuppressWarnings("rawtypes")
	private void loadSettings() {
		FileInputStream fis;
        try {
        	LocationJar locationJar = new LocationJar();
    		String path = locationJar.getLocationJar();
    		String caminhoArquivoCompleto = path + file;
            fis = new FileInputStream(caminhoArquivoCompleto);
            propriedades.load(fis);
            Set keySet = propriedades.keySet(); 
            Iterator iterator = keySet.iterator();
            while (iterator.hasNext()) {
            	String chave = (String) iterator.next();
            	System.setProperty(chave, propriedades.getProperty(chave));
            }
            fis.close();
        } catch (FileNotFoundException e) {
        	System.out.println("arquivo de configuracao nao encontrado, o sistema utilizara as configuracoes padrao");
        } catch (IOException e) {
        	System.out.println("Erro ao carregar propriedades do arquivo, o sistema utilizara as configuracoes padrao");
        }
	}

    public int getPropriedadeNumerica(String chave, int valorPadrao) {
        String value = System.getProperty(chave, String.valueOf(valorPadrao));
        int retorno = valorPadrao;
        try {
            retorno = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
        }
        return retorno;
    }
    
    public boolean getPropriedadeBoolean(String chave, boolean valorPadrao) {
        String value = System.getProperty(chave, String.valueOf(valorPadrao));
        boolean retorno = valorPadrao;
        try {
            retorno = Boolean.parseBoolean(value);
        } catch (NumberFormatException ex) {
        }
        return retorno;
    }

    public String getPropriedadeString(String chave, String valorPadrao) {
        return System.getProperty(chave, valorPadrao);
    }
    
    private static class PropriedadesUtilHolder {
		private static final PropertiesUtil INSTANCE = new PropertiesUtil();
	}
	
	public static PropertiesUtil getInstance() {
		return PropriedadesUtilHolder.INSTANCE;
	}
}
