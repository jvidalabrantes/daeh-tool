package br.ufrn.lets.daeh.util;

import java.io.File;

/**
 * Localiza a pasta onde está sendo executado o cliente DAEH.
 * @author Joilson Abrantes
 *
 */
public class LocationJar {
	
	public String getLocationJar() {
	    String name = getClass().getName().replace('.', '/');
	    name = getClass().getResource("/" + name + ".class").toString();
	    name = name.substring(0, name.indexOf(".jar"));
	    name = name.substring(name.lastIndexOf(':')-1, name.lastIndexOf('/')+1).replace('%', ' ');
	    String s = "";
	    for (int k=0; k<name.length(); k++) {
	      s += name.charAt(k);
	      if (name.charAt(k) == ' ') k += 2;
	    }
	    if (s.startsWith("e:")) {
	    	s = s.substring(2, s.length() -1);
	    }
	    s = s.replace('/', File.separatorChar);
	    return (s + File.separatorChar);
		//return "C:/Users/Joilson/git/daeh/FEAR_LIB/";
	}  
	      
}
