/*
 * ------------------------------------------------------
 * | GITS - Grupo de Interesse em Testes de Software     |
 * | F.E.A.R - Framework for Exception Aware Refactoring |
 * ------------------------------------------------------
 */

/**
 * Pacote criado para armazenar classes de utilidade.
 * Elas realizam operacoes auxiliares as entidades
 * @author Ricardo Sales
 */

package br.ufrn.lets.daeh.util;

