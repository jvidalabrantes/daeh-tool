package br.ufrn.lets.daeh.util;

/**
 * 
 * Propriedades do DAEH monitor.
 * 
 * @author Joilson Abrantes
 *
 */
public class PropertiesDAEH {
	
	
	public static String getIdentificadorAplicacao() {
        return PropertiesUtil.getInstance().getPropriedadeString("servidor.daeh.identificador.aplicacao", "localhost");
    }
	
    public static String getHostServidorDAEH() {
        return PropertiesUtil.getInstance().getPropriedadeString("servidor.daeh.host", "localhost");
    }
    
    public static int getPortaServidorDAEH() {
        return PropertiesUtil.getInstance().getPropriedadeNumerica("servidor.daeh.porta", 80);
    }
    
    public static boolean getHabilitadoServidorDAEH() {
        return PropertiesUtil.getInstance().getPropriedadeBoolean("servidor.daeh.habilitado", false);
    }
}
