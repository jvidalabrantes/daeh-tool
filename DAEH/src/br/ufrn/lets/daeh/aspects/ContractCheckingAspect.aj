package br.ufrn.lets.daeh.aspects;


import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import br.ufrn.lets.daeh.comunication.ComunicationServer;
import br.ufrn.lets.daeh.controller.ContractController;
import br.ufrn.lets.daeh.exceptions.ContractCheckingException;
import br.ufrn.lets.daeh.exceptions.MethodNotFoundException;
import br.ufrn.lets.daeh.util.LocationJar;
import br.ufrn.lets.daeh.util.MacAddressUtil;
import br.ufrn.lets.daeh.util.PropertiesDAEH;
import br.ufrn.lets.daeh.thread.ManagerThreads;


/**
 * Aspecto que verifica em tempo de execução se os contratos excepcionais estão sendo obedecidos
 * @author Joilson Abrantes
 */
public aspect ContractCheckingAspect{
	
	
	/**
	 * Inicializa o controlador passando como parâmetro o arquivo de contrato.
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @throws JClassAlreadyExistsException 
	 * @throws URISyntaxException 
	 */
	public ContractCheckingAspect() throws IOException, ClassNotFoundException, URISyntaxException{
	
		
		LocationJar locationJar = new LocationJar();
		String path = locationJar.getLocationJar();
		System.out.println("Caminho do Arquivo: " + path);
		path = path +"rules"+File.separatorChar+"contract.xml";
		System.out.println("Caminho do Arquivo:" + path);
		
		File f = new File(path);
		ContractController.getInstance().loadContracts(f);
		
		boolean habilitadorServidor = PropertiesDAEH.getHabilitadoServidorDAEH();
		
		if (habilitadorServidor) {
			String mac = MacAddressUtil.getMacAddress(); 
			ComunicationServer comunicationServer = new ComunicationServer();
			comunicationServer.saveHostInformation(mac);
		}
		
	}
	
	
	/**
	 * Pointcut que intercepta as capturas dos diversos tipos de 
	 * exceção [{@link Throwable}, {@link Exception}, {@link RuntimeException} e respectivas subclasses] 
	 * e que nao estejam dentro do aspecto de contrato ou do controlador.
	 * @param exception
	 */
	pointcut exceptionHandlingContract (Throwable exception) : (handler (Exception+) || handler(RuntimeException+) || handler(Throwable+)) && !within(ContractCheckingAspect)
		&& !cflow(execution(* br.ufrn.lets.daeh.entities.Contract.*(..)))  &&  !cflow(execution(* br.ufrn.lets.daeh.controller.ContractController.*(..))) && args(exception);
	
	
	/**
	 * Método para verificar se o contrato esta sendo obedecido 
	 * quando uma exceçõã é capturada dentro da aplicação
	 * @param exception Excecao que foi capturada durante 
	 * a execução do programa
	 * @throws MethodNotFoundException 
	 * @throws ContractCheckingException 
	 * @throws ClassNotFoundException 
	 */
	before (Throwable exception) throws ContractCheckingException, MethodNotFoundException, ClassNotFoundException : exceptionHandlingContract(exception){
		
		org.aspectj.lang.Signature handlerSignature = thisEnclosingJoinPointStaticPart.getSignature();
		ManagerThreads.getInstance().createCheckRule(handlerSignature, exception);
	}
	
}
