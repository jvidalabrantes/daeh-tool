/**
 * Pacote criado para armazenar o aspecto principal do framework
 * que quando ocorre o tratamento de uma exceção, verifica se ela obedece o contrato
 * @author Joilson Abrantes
 */

package br.ufrn.lets.daeh.aspects;

