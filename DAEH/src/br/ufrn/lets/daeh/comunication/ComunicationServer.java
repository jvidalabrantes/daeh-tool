package br.ufrn.lets.daeh.comunication;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.lets.monitor.commons.recursos.RequisicaoRecurso;
import br.ufrn.lets.daeh.util.MacAddressUtil;
import br.ufrn.lets.daeh.util.PropertiesDAEH;
import br.ufrn.lets.minerador.comunicacao.dominio.HostEvent;
import br.ufrn.lets.minerador.comunicacao.dominio.HostEventReport;
import br.ufrn.lets.minerador.comunicacao.dominio.HostInformation;
import br.ufrn.lets.minerador.comunicacao.dominio.HostInformationReport;

/**
 * 
 * Responsável pela o envio das informações de monitoramento ao servidor.
 * 
 * @author Joilson Abrantes
 *
 */
public class ComunicationServer {
	
	/**
	 * Envia o endereço do host que está usando a ferramenta de monitoramento.
	 * @param mac
	 */
	public void saveHostInformation(String mac) {
		
		String host = PropertiesDAEH.getHostServidorDAEH();
		int porta = PropertiesDAEH.getPortaServidorDAEH();
		String identificadorAplicacao = PropertiesDAEH.getIdentificadorAplicacao();
		
		HostInformation hostInformation = new HostInformation();
		hostInformation.setIp(mac);
		HostInformationReport hostInformationReport = new HostInformationReport(identificadorAplicacao, hostInformation);
		
		RequisicaoRecurso requisicao = new RequisicaoRecurso(host, porta);
		
		try {
			requisicao.saveHostInfo(hostInformationReport);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Envia a captura de uma exceção para o servidor de monitorameto.
	 * @param exception
	 * @param exceptionMessage
	 */
	public void saveEvent(Throwable exception, String exceptionMessage) {
		
		try {
			String host = PropertiesDAEH.getHostServidorDAEH();
			int porta = PropertiesDAEH.getPortaServidorDAEH();
			String mac = MacAddressUtil.getMacAddress(); 
	
			HostEvent hostEvent = new HostEvent();
			hostEvent.setDate(new Date());
			hostEvent.setLog(exceptionMessage);
			List<HostEvent> hostEvents = new ArrayList<HostEvent>();
			hostEvents.add(hostEvent);
			HostEventReport hostEventReport = new HostEventReport(mac, hostEvents);
			RequisicaoRecurso requisicao = new RequisicaoRecurso(host, porta);
			requisicao.saveHostEvent(hostEventReport);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
