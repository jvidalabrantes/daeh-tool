/*
 * ------------------------------------------------------
 * | GITS - Grupo de Interesse em Testes de Software     |
 * | F.E.A.R - Framework for Exception Aware Refactoring |
 * ------------------------------------------------------
 */

package br.ufrn.lets.daeh.exceptions;


/**
 * Exceção a ser lançada quando um método indicado no contrato não for encontrado.
 * 
 * @author Joilson Abrantes
 */
public class MethodNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 2L;
	
	public MethodNotFoundException (String message){
		super (message);
	}
}
