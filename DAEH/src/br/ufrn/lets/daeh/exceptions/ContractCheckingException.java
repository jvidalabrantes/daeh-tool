package br.ufrn.lets.daeh.exceptions;


/**
 * Exceção a ser usada pelo framework quando 
 * o existe um erro na checagem do contrato.
 * 
 * @author Joilson Abrantes
 *
 */
public class ContractCheckingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ContractCheckingException(String message){
		super(message);
	}
}
